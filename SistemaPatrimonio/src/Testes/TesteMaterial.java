
package Testes;

import Model.bean.Material;
import Model.dao.MaterialDAO;
import java.util.List;

/**
 *
 * @author Jean Ferreira
 */
public class TesteMaterial {
    
    public static void main(String[] args){
        
        List<Material> materiais;
        //Material material = new Material();
        MaterialDAO mDao = new MaterialDAO();
        
        materiais = mDao.selecionarMateriais("CABO ELÉTRICO 1X40MM² - COR VERMELHO");
        
        materiais.forEach((material)->{
            System.out.println(material.getIdUnidadeMedida().getTipo());
        });
    }
}
