package Model.dao;

import Connection.ConnectionFactory;
import Model.bean.Administrador;
import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AdministradorDAO {
    private static Connection con = null;
    PreparedStatement stmt = null;
    ResultSet rs = null;
        
    public boolean validarLogin(Administrador adm){
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM administrador WHERE siape = \""+adm.getSiape()+"\" AND senha = \""+adm.getSenha()+"\";");
            rs = stmt.executeQuery();
            
            if(rs.next())
                return true;
            return false;
        } catch (SQLException ex) {
            System.out.println("Usuário não encontrado.\n"+ex);
            return false;
        }finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }
    }
    
    public boolean atualizarAdministrador(Administrador adm, int siape){
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("UPDATE administrador SET siape = ?, nome = ?, senha = ? WHERE siape = ?");
            stmt.setInt(1, Integer.parseInt(adm.getSiape()));
            stmt.setString(2, adm.getNome());
            stmt.setString(3, adm.getSenha());
            stmt.setInt(4, siape);
            stmt.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(AdministradorDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }
        return false;
    }
    
    public Administrador selecionarAdministrador(Administrador adm){
        con = ConnectionFactory.getConnection();
        Administrador administrador = new Administrador();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM administrador WHERE siape = ? AND senha = ?;");
            stmt.setInt(1, Integer.parseInt(adm.getSiape()));
            stmt.setString(2, adm.getSenha());
            rs = stmt.executeQuery();
            
            if(rs.next()){
                administrador.setSiape(String.valueOf(rs.getInt("siape")));
                administrador.setNome(rs.getString("nome"));
                administrador.setSenha(rs.getString("senha"));
            }
            
            return administrador;
        } catch (SQLException ex) {
            System.out.println("Usuário não encontrado.\n"+ex);
            return null;
        }finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }
    }
}
