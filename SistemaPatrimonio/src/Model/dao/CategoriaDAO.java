package Model.dao;

import Connection.ConnectionFactory;
import Model.bean.Categoria;
import com.mysql.jdbc.PreparedStatement;
import java.awt.Component;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class CategoriaDAO {
    private Connection con;
    private PreparedStatement stmt = null;
    private ResultSet rs = null;
    
    public void cadastrarCategoria (Categoria categoria){
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("INSERT INTO categoria (idcategoria, nome) VALUES (?,?);");
            stmt.setString(1,null);
            stmt.setString(2,categoria.getNome());
            
            stmt.executeUpdate();
            
            JOptionPane.showMessageDialog(null,"Categoria inserida com sucesso.\n");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao inserir nova categoria.\n"+ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt);
        }
    }
    
    public List<Categoria> selecionarCategorias(){
        List<Categoria> Categoria = new ArrayList<>();
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM categoria ORDER BY nome;");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                Categoria categoria = new Categoria();
                categoria.setId(rs.getInt("idcategoria"));
                categoria.setNome(rs.getString("nome"));
                Categoria.add(categoria);
            }
            return Categoria;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(getParent(), "Erro ao pegar categorias!"," ALERTA",JOptionPane.ERROR_MESSAGE);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
        return Categoria;
    }

    private Component getParent() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Categoria selecionarCategoriaPorNome(String nome){
        Categoria categoria = new Categoria();
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM categoria WHERE nome = \""+nome+"\";");
            rs = stmt.executeQuery();
            
            rs.first();
            
            categoria.setId(rs.getInt("idcategoria"));
            categoria.setNome(rs.getString("nome"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(getParent(), "Erro ao pegar categoria!\n"+ex," ALERTA",JOptionPane.ERROR_MESSAGE);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
        return categoria;
    }
    
    public int pegarQuantidade(){
        con = ConnectionFactory.getConnection();
        int qtd=0;
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT COUNT(*) AS total FROM categoria;");
            rs = stmt.executeQuery();
            
            rs.first();
            qtd = rs.getInt("total");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(getParent(), "Erro ao pegar quantidade!\n"+ex," ALERTA",JOptionPane.ERROR_MESSAGE);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
        return qtd;
    }
    
    public int editarCategoria(Categoria categoria){
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("UPDATE categoria SET nome = ? WHERE idcategoria = ?;");
            stmt.setString(1,categoria.getNome());
            stmt.setInt(2,categoria.getId());
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null,"Categoria atualizada com sucesso.\nAtualize a tabela.");
            return 0;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(getParent(), "Não foi possível atualizar categoria!\n"+ex," ALERTA",JOptionPane.ERROR_MESSAGE);
            return 1;
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
    }
}
