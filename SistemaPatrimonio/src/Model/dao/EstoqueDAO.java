package Model.dao;

import Connection.ConnectionFactory;
import Model.bean.Estoque;
import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class EstoqueDAO {
    private Connection con;
    private PreparedStatement stmt;
    private ResultSet rs;
    
    public boolean salvar(Estoque estoque){
        con = ConnectionFactory.getConnection();
        stmt = null;
        try {
            stmt = (PreparedStatement) con.prepareStatement("INSERT INTO estoque (idEstoque, quantidade, saida_idSaida,"
                    + " entrada_nNotaFiscal, material_idMaterial)"
                    + "VALUES (?,?,?,?,?);");
            stmt.setString(1,null);
            stmt.setInt(2,estoque.getQuantidade());
            stmt.setString(3,null);
            stmt.setString(4,estoque.getEntrada_nNotaFiscal().getNotaFiscal());
            stmt.setInt(5,estoque.getMaterial_idMaterial().getIdMaterial());
            
            stmt.executeUpdate();
            somar(estoque);
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao salvar material no estoque."+ex);
            return false;
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt);
        }
    }
    
    public boolean somar(Estoque estoque){
        con = ConnectionFactory.getConnection();
        stmt = null;
        rs = null;
        
        Estoque estoque2 = new Estoque();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT quantidade FROM estoque WHERE material_idMaterial = ?;");
            stmt.setInt(1, estoque.getMaterial_idMaterial().getIdMaterial());
            rs = stmt.executeQuery();
            rs.first();
            
            estoque2.setQuantidade(rs.getInt("quantidade"));
            
            int soma = (estoque2.getQuantidade())+(estoque.getEntrada_nNotaFiscal().getQuantidade());
            
            try {
                stmt = (PreparedStatement) con.prepareStatement("UPDATE estoque SET quantidade = ? WHERE material_idMaterial = ?;");
                stmt.setInt(1, soma);
                stmt.setInt(2, estoque.getMaterial_idMaterial().getIdMaterial());
            
                stmt.executeUpdate();
                return true;
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null,"Erro ao atualizar estoque.\n"+ex);
                return false;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao pegar quantidade do estoque.\n"+ex);
            return false;
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }
    }
    
    public boolean remover(Estoque estoque){
        con = ConnectionFactory.getConnection();
        
        Estoque estoque2 = new Estoque();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT quantidade FROM estoque "
                    + "WHERE material_idMaterial = \""+estoque.getMaterial_idMaterial().getIdMaterial()+"\" AND quantidade > 0;");
            rs = stmt.executeQuery();
            rs.first();
            
            estoque2.setQuantidade(rs.getInt("quantidade"));
            
            int subtracao = (estoque2.getQuantidade())-(estoque.getSaida_idSaida().getQuantidade());
            
            try {
                stmt = (PreparedStatement) con.prepareStatement("UPDATE estoque SET quantidade = ? WHERE material_idMaterial = \""+estoque.getMaterial_idMaterial().getIdMaterial()+"\";");
                stmt.setInt(1,subtracao);

                stmt.executeUpdate();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null,"Erro ao subtrair do banco\n"+ex);
            }
            //JOptionPane.showMessageDialog(null,"Estoque atualizado");
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao pegar total do estoque\n"+ex);
            return false;
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }           
    }
    
    public boolean quantidadeSolicitada(String material, int qtdDesejada, int qtdAdd){
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT quantidade FROM estoque INNER JOIN material ON estoque.material_idMaterial = material.idMaterial WHERE descricao = ? AND quantidade - (?+?) >= 0");
            stmt.setString(1, material);
            stmt.setInt(2, qtdDesejada);
            stmt.setInt(3, qtdAdd);
            rs = stmt.executeQuery();

            if(rs.next()){
                return true;
            } else{
                return false;
            }
        } catch (SQLException ex) {
            System.err.print(ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }    
        return false;
    }
}
