package Model.dao;

import Connection.ConnectionFactory;
import Model.bean.Entrada;
import Model.bean.Fornecedor;
import Model.bean.Material;
import Model.bean.TipoAquisicao;
import com.mysql.jdbc.PreparedStatement;
import java.awt.Component;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class EntradaDAO {
    private Connection con;
    private ResultSet rs;
    private PreparedStatement stmt = null;
    
    public boolean efetuarEntrada(Entrada entrada){
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("INSERT INTO entrada"
                    + "(nNotaFiscal,codBarras,quantidade,dataEntrada,tipoAquisicao_idtipoAquisicao,fornecedor_cnpj,material_idMaterial)"
                    + "VALUES (?,?,?,?,?,?,?);");
            
            stmt.setString(1,entrada.getNotaFiscal());
            stmt.setString(2,entrada.getCodBarras());
            stmt.setInt(3,entrada.getQuantidade());
            stmt.setString(4,entrada.getData());
            stmt.setInt(5,entrada.getTipoAquisicaoId().getId());
            stmt.setString(6,entrada.getFornecedorCnpj().getCnpj());
            stmt.setInt(7,entrada.getMaterial_idMaterial().getIdMaterial());
            
            stmt.executeUpdate();
            
            JOptionPane.showMessageDialog(null,"Entrada inserida com sucesso\n");
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao fazer entrada de material\n"+ex);
            return false;
        }finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt);
        }
    }
    
    public List<Entrada> consultarEntradas(){
        List<Entrada> Entrada = new ArrayList<>();
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT entrada.dataEntrada, material.descricao, entrada.quantidade, fornecedor.nome, "
                    + "fornecedor.cnpj, entrada.nNotaFiscal, tipoaquisicao.descricao FROM entrada INNER JOIN material ON "
                    + "entrada.material_idMaterial = material.idMaterial LEFT JOIN fornecedor ON entrada.fornecedor_cnpj = "
                    + "fornecedor.cnpj INNER JOIN tipoaquisicao ON entrada.tipoaquisicao_idtipoAquisicao = tipoaquisicao.idtipoAquisicao "
                    + "ORDER BY entrada.dataEntrada DESC;");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                Entrada entrada = new Entrada();
                Material material = new Material();
                Fornecedor fornecedor = new Fornecedor();
                TipoAquisicao tipo = new TipoAquisicao();
                
                entrada.setData(rs.getString("entrada.dataEntrada"));
                material.setDescricao(rs.getString("material.descricao"));
                entrada.setQuantidade(rs.getInt("entrada.quantidade"));
                fornecedor.setNome(rs.getString("fornecedor.nome"));
                fornecedor.setCnpj(rs.getString("fornecedor.cnpj"));
                entrada.setNotaFiscal(rs.getString("entrada.nNotaFiscal"));
                tipo.setDescricao(rs.getString("tipoaquisicao.descricao"));
                entrada.setMaterial_idMaterial(material);
                entrada.setTipoAquisicaoId(tipo);
                entrada.setFornecedorCnpj(fornecedor);
                Entrada.add(entrada);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao listar entradas\n"+ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }
        return Entrada;
    }
    
    public Entrada selecionarEntradaPorNotaFiscal(String sqlNotaFiscal){
        Entrada entrada = new Entrada();
        Material material = new Material();
        Fornecedor fornecedor = new Fornecedor();
        TipoAquisicao tipo = new TipoAquisicao();
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT entrada.dataEntrada, material.descricao, entrada.quantidade, fornecedor.nome, "
                    + "fornecedor.cnpj, entrada.nNotaFiscal, tipoaquisicao.descricao FROM entrada INNER JOIN material ON "
                    + "entrada.material_idMaterial = material.idMaterial LEFT JOIN fornecedor ON entrada.fornecedor_cnpj = "
                    + "fornecedor.cnpj INNER JOIN tipoaquisicao ON entrada.tipoaquisicao_idtipoAquisicao = tipoaquisicao.idtipoAquisicao WHERE entrada.nNotaFiscal = "+sqlNotaFiscal+";");
            rs = stmt.executeQuery();
            
            rs.first();

            entrada.setData(rs.getString("entrada.dataEntrada"));
            material.setDescricao(rs.getString("material.descricao"));
            entrada.setQuantidade(rs.getInt("entrada.quantidade"));
            fornecedor.setNome(rs.getString("fornecedor.nome"));
            fornecedor.setCnpj(rs.getString("fornecedor.cnpj"));
            entrada.setNotaFiscal(rs.getString("entrada.nNotaFiscal"));
            tipo.setDescricao(rs.getString("tipoaquisicao.descricao"));
            entrada.setMaterial_idMaterial(material);
            entrada.setTipoAquisicaoId(tipo);
            entrada.setFornecedorCnpj(fornecedor);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao pegar entrada\n"+ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }
        return entrada;
    }
    
    public void editarEntrada(Entrada entrada){
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("UPDATE entrada SET codBarras = ?, quantidade = ?, dataEntrada = ?, "
                    + "tipoAquisicao_idtipoAquisicao = ?, fornecedor_cnpj = ?, material_idMaterial = ? WHERE nNotaFiscal = ?");
            
            stmt.setString(1, entrada.getCodBarras());
            stmt.setInt(2, entrada.getQuantidade());
            stmt.setString(3, entrada.getData());
            stmt.setInt(4, entrada.getTipoAquisicaoId().getId());
            stmt.setString(5, entrada.getFornecedorCnpj().getCnpj());
            stmt.setInt(6, entrada.getMaterial_idMaterial().getIdMaterial());
            stmt.setString(7, entrada.getNotaFiscal());
            
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null,"Atualização feita com sucesso.");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(getParent(), "Erro ao atualizar!"," ALERTA",JOptionPane.ERROR_MESSAGE);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }
    }

    private Component getParent() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public int pegarQuantidade(){
        con = ConnectionFactory.getConnection();
        int qtd=0;
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT COUNT(*) AS total FROM entrada;");
            rs = stmt.executeQuery();
            rs.first();
            qtd = rs.getInt("total");
        } catch (SQLException ex) {
            Logger.getLogger(EntradaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }
        return qtd;
    }
}