package Model.dao;

import Connection.ConnectionFactory;
import Model.bean.UnidadeDeMedida;
import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class UnidadeDeMedidaDAO {
    private Connection con;
    private ResultSet rs;
    private PreparedStatement stmt = null;
    
    public void cadastrarUnidadeDeMedida (UnidadeDeMedida unidade){
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("INSERT INTO unidadeMedida (idunidadeMedida, tipo) VALUES (?,?);");
            stmt.setString(1,null);
            stmt.setString(2,unidade.getTipo());
            
            stmt.executeUpdate();
            
            JOptionPane.showMessageDialog(null,"Unidade de medida inserida com sucesso.");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao inserir unidade de medida\n"+ex);
        } finally{
          ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt);
        }
    }
    
    public List<UnidadeDeMedida> selecionarUnidadesDeMedida(){
        List<UnidadeDeMedida> Unidade = new ArrayList<>();
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM unidadeMedida ORDER BY tipo;");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                UnidadeDeMedida unidade = new UnidadeDeMedida();
                unidade.setId(rs.getInt("idunidadeMedida"));
                unidade.setTipo(rs.getString("tipo"));
                Unidade.add(unidade);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UnidadeDeMedidaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
        return Unidade;
    }
    
    public UnidadeDeMedida selecionarUnidadeDeMedidaPorNome(String nome){
        UnidadeDeMedida unidade = new UnidadeDeMedida();
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM unidadeMedida WHERE tipo = \""+nome+"\";");
            rs = stmt.executeQuery();
            
            rs.first();
            unidade.setId(rs.getInt("idunidadeMedida"));
            unidade.setTipo(rs.getString("tipo"));
        } catch (SQLException ex) {
            Logger.getLogger(UnidadeDeMedidaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
        return unidade;
    }
    
    public int editarUnidadeDeMedida(UnidadeDeMedida unidade){
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("UPDATE unidadeMedida SET tipo = ? WHERE idunidadeMedida = ?");
            stmt.setString(1, unidade.getTipo());
            stmt.setInt(2,unidade.getId());
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null,"Unidade de medida atualizada com sucesso.\nAtualize a tabela.");
        } catch (SQLException ex) {
            Logger.getLogger(UnidadeDeMedidaDAO.class.getName()).log(Level.SEVERE, null, ex);
            return 1;
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
        return 0;
    }
    
    public int pegarQuantidade(){
        con = ConnectionFactory.getConnection();
        int qtd = 0;
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT COUNT(*) AS total FROM unidadeMedida;");
            rs = stmt.executeQuery();
            rs.first();
            qtd = rs.getInt("total");
        } catch (SQLException ex) {
            Logger.getLogger(UnidadeDeMedidaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
        return qtd;
    }
}
