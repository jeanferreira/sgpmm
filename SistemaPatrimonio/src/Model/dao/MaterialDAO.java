package Model.dao;

import Connection.ConnectionFactory;
import Model.bean.Categoria;
import Model.bean.Material;
import Model.bean.UnidadeDeMedida;
import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class MaterialDAO {
    private static Connection con;
    private PreparedStatement stmt = null;
    private ResultSet rs;
    
    public final void cadastrarMaterial(Material material){
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("INSERT INTO material (idMaterial, descricao, qtd_minima, unidadeMedida_idunidadeMedida,categoria_idcategoria) VALUES (?,?,?,?,?);");
            stmt.setString(1,null);
            stmt.setString(2,material.getDescricao());
            stmt.setInt(3,material.getQtd_minima());
            stmt.setInt(4,material.getIdUnidadeMedida().getId());
            stmt.setInt(5,material.getCategoriaId().getId());
            
            stmt.executeUpdate();
            
            JOptionPane.showMessageDialog(null,"Material cadastrado com sucesso!\n");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao cadastrar novo material!\n"+ ex);
        }finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt);
        }
    }
    
    public final List<Material> selecionarMateriais(){
        con = ConnectionFactory.getConnection();
        List<Material> Materiais = new ArrayList<>();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM material INNER JOIN categoria ON material.categoria_idcategoria = categoria.idcategoria "
                    + "INNER JOIN unidadeMedida ON material.unidadeMedida_idunidadeMedida = unidadeMedida.idunidadeMedida "
                    + "ORDER BY material.descricao;");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                Material material = new Material();
                Categoria categoria = new Categoria();
                UnidadeDeMedida unidade = new UnidadeDeMedida();
                
                material.setIdMaterial(rs.getInt("material.idMaterial"));
                material.setDescricao(rs.getString("material.descricao"));
                material.setQtd_minima(rs.getInt("material.qtd_minima"));
                
                categoria.setId(rs.getInt("material.categoria_idcategoria"));
                categoria.setNome(rs.getString("categoria.nome"));
                material.setCategoriaId(categoria);
                
                unidade.setId(rs.getInt("material.unidadeMedida_idunidadeMedida"));
                unidade.setTipo(rs.getString("unidadeMedida.tipo"));
                material.setIdUnidadeMedida(unidade);
                
                Materiais.add(material);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao pegar materiais!\n"+ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt);
        }
        return Materiais;
    }
    
    public final List<Material> selecionarMateriais(String descricao){
        con = ConnectionFactory.getConnection();
        List<Material> materiais = new ArrayList<>();
        
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM material INNER JOIN categoria ON material.categoria_idcategoria = "
                    + "categoria.idcategoria INNER JOIN unidadeMedida ON material.unidadeMedida_idunidadeMedida = "
                    + "unidadeMedida.idunidadeMedida WHERE material.descricao LIKE '"+descricao+"%' ORDER BY material.descricao;");
            rs = stmt.executeQuery();
           
            int i=0;
            while(rs.next() && i < 8){
                Material material = new Material();
                Categoria categoria = new Categoria();
                UnidadeDeMedida unidade = new UnidadeDeMedida();
                
                material.setIdMaterial(rs.getInt("material.idMaterial"));
                material.setDescricao(rs.getString("material.descricao"));
                material.setQtd_minima(rs.getInt("material.qtd_minima"));

                categoria.setId(rs.getInt("categoria.idcategoria"));
                categoria.setNome(rs.getString("categoria.nome"));
                material.setCategoriaId(categoria);

                unidade.setId(rs.getInt("unidadeMedida.idunidadeMedida"));
                unidade.setTipo(rs.getString("unidadeMedida.tipo"));
                material.setIdUnidadeMedida(unidade);
                
                materiais.add(material);
                i++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(MaterialDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }
        return materiais;
    }
    
    public Material selecionar(String descricao){
        con = ConnectionFactory.getConnection();
        
        Material material = new Material();
        Categoria categoria = new Categoria();
        UnidadeDeMedida unidade = new UnidadeDeMedida();
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM material INNER JOIN categoria ON material.categoria_idcategoria = "
                    + "categoria.idcategoria INNER JOIN unidadeMedida ON material.unidadeMedida_idunidadeMedida = "
                    + "unidadeMedida.idunidadeMedida WHERE material.descricao = \""+descricao+"\";");
            rs = stmt.executeQuery();
            rs.first();
            
            material.setIdMaterial(rs.getInt("material.idMaterial"));
            material.setDescricao(rs.getString("material.descricao"));
            material.setQtd_minima(rs.getInt("material.qtd_minima"));

            categoria.setId(rs.getInt("categoria.idcategoria"));
            categoria.setNome(rs.getString("categoria.nome"));
            material.setCategoriaId(categoria);

            unidade.setId(rs.getInt("unidadeMedida.idunidadeMedida"));
            unidade.setTipo(rs.getString("unidadeMedida.tipo"));
            material.setIdUnidadeMedida(unidade);
        } catch (SQLException ex) {
            Logger.getLogger(MaterialDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return material;
    }
    
    public final int pegarQuantidade(){
        con = ConnectionFactory.getConnection();
        int qtd = 0;
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT COUNT(*) AS total FROM material;");
            rs = stmt.executeQuery();
            rs.first();
            qtd = rs.getInt("total");
        } catch (SQLException ex) {
            Logger.getLogger(MaterialDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt);
        }
        return qtd;
    }
    
    public final boolean editarMaterial(Material material){
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("UPDATE material SET descricao = ?, qtd_minima = ?, categoria_idcategoria = ?, unidadeMedida_idunidadeMedida = ? "
                    + "WHERE idMaterial = ?;");
            stmt.setString(1, material.getDescricao());
            stmt.setInt(2, material.getQtd_minima());
            stmt.setInt(3, material.getCategoriaId().getId());
            stmt.setInt(4, material.getIdUnidadeMedida().getId());
            stmt.setInt(5, material.getIdMaterial());
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null,"Material atualizado com sucesso.");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MaterialDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt);
        }
    }
    
    public final void deletarMaterial(Material material){
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("DELETE FROM material WHERE idMaterial = "+material.getIdMaterial()+"");/* "
                    + "AND material.unidadeMedida_idunidadeMedida = "+material.getIdUnidadeMedida().getId()+" AND "
                            + "material.categoria_idcategoria = "+material.getCategoriaId().getId()+";");*/
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null,"Material excluído com sucesso.");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Não foi possível excluir material!"+ex);
        }
    }
        
    //Método responsável por verificar se o material desejado para exclusão existe no banco
    //de dados, caso exista retorna verdadeiro.
    public final boolean verificarEstoque(Material material){
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM estoque WHERE material_idMaterial = "+material.getIdMaterial()+";");
            rs = stmt.executeQuery();
            
            if(rs.first()){
                return true;
            } else{
                return false;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Não foi possível verificar material no estoque!"+ex);
            return false;
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }
    }
}