/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.dao;

import Connection.ConnectionFactory;
import Model.bean.Solicitante;
import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Jean Ferreira
 */
public class SolicitanteDAO {
    private Connection con;
    private PreparedStatement stmt;
    private ResultSet rs;
    
    public boolean cadastrarSolicitante(Solicitante solicitante){
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("INSERT INTO solicitante (id,nome,setor) VALUES (?,?,?);");
            stmt.setString(1,null);
            stmt.setString(2,solicitante.getNome());
            stmt.setString(3,solicitante.getSetor());
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null,"Solicitante cadastrado com sucesso!");
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao cadastrar solicitante\n"+ex);
            return false;
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt);
        }
    }
    
    public List<Solicitante> selecionarSolicitantes(){
        List<Solicitante> Solicitante = new ArrayList<>();
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM solicitante;");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                Solicitante solicitante = new Solicitante();
                solicitante.setId(rs.getInt("id"));
                solicitante.setNome(rs.getString("nome"));
                solicitante.setSetor(rs.getString("setor"));
                Solicitante.add(solicitante);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SolicitanteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
        return Solicitante;
    }
    
    public Solicitante selecionarSolicitantePorNome(String nome){
        Solicitante solicitante = new Solicitante();
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM solicitante WHERE nome = \""+nome+"\";");
            rs = stmt.executeQuery();
            rs.first();
            solicitante.setId(rs.getInt("id"));
            solicitante.setNome(rs.getString("nome"));
            solicitante.setSetor(rs.getString("setor"));
        } catch (SQLException ex) {
            Logger.getLogger(SolicitanteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
        return solicitante;
    }
    
    public List<Solicitante> selecionarSolicitantes(String nome){
        List<Solicitante> solicitantes = new ArrayList<>();
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM solicitante WHERE nome LIKE '"+nome+"%' ORDER BY nome;");
            rs = stmt.executeQuery();
            
            int i=0;
            while(rs.next() && 1 < 8){
                Solicitante sol = new Solicitante();
                sol.setId(rs.getInt("id"));
                sol.setNome(rs.getString("nome"));
                sol.setSetor(rs.getString("setor"));
                solicitantes.add(sol);
                i++;
            }
            return solicitantes;
        } catch (SQLException ex) {
            Logger.getLogger(SolicitanteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public int pegarQuantidade(){
        int qtd = 0;
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT COUNT(*) AS total FROM solicitante;");
            rs = stmt.executeQuery();
            rs.first();
            qtd = rs.getInt("total");
        } catch (SQLException ex) {
            Logger.getLogger(SolicitanteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
        return qtd;
    }
    
    public boolean editarSolicitante(Solicitante solicitante){
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("UPDATE solicitante SET nome = ?, setor = ? WHERE id = ?");
            stmt.setString(1,solicitante.getNome());
            stmt.setString(2,solicitante.getSetor());
            stmt.setInt(3,solicitante.getId());
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null,"Solicitante atualizado com sucesso.");
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao atualizar solicitante!\n"+ex);
            return false;
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
    }
}
