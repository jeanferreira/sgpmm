package Model.dao;

import Connection.ConnectionFactory;
import Model.bean.Fornecedor;
import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class FornecedorDAO {
    private static Connection con;
    private PreparedStatement stmt;
    private ResultSet rs;
    
    public boolean cadastrarFornecedor(Fornecedor fornecedor){
        con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("INSERT INTO fornecedor (cnpj,nome, email, telefone) VALUES (?,?,?,?);");
     
            stmt.setString(1,fornecedor.getCnpj());
            stmt.setString(2,fornecedor.getNome());
            stmt.setString(3,fornecedor.getEmail());
            stmt.setString(4,fornecedor.getTelefone());

            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null,"Fornecedor cadastrado com sucesso!\n");
            return true;
        } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null,"Erro ao cadastrar fornecedor!\n"+ ex);
           return false;
        }finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt);
        }
    }
    
    public boolean editarFornecedor(Fornecedor fornecedor){
        con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("UPDATE fornecedor SET nome=?, email=?, telefone=? WHERE cnpj=?");
            stmt.setString(1,fornecedor.getNome());
            stmt.setString(2,fornecedor.getEmail());
            stmt.setString(3,fornecedor.getTelefone());
            stmt.setString(4,fornecedor.getCnpj());
            
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null,"Informações atualizadas com sucesso!\n");
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao atualizar informações!\n"+ ex);
            return false;
        }finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt);
        }
    }
    
    public void deletarFornecedor(Fornecedor fornecedor){
        con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("DELETE FROM fornecedor WHERE cnpj=?");
            stmt.setString(1,fornecedor.getCnpj());
            
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null,"Fornecedor deletado com sucesso!\n");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao deletar fornecedor!\n"+ ex);
        }finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt);
        }
    }
    
    public List<Fornecedor> selecionarFornecedores(){
        con = ConnectionFactory.getConnection();
        
        List<Fornecedor> fornecedores = new ArrayList<>();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM fornecedor ORDER BY nome;");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                Fornecedor fornecedor = new Fornecedor();
                
                fornecedor.setCnpj(rs.getString("cnpj"));
                fornecedor.setNome(rs.getString("nome"));
                fornecedor.setEmail(rs.getString("email"));
                fornecedor.setTelefone(rs.getString("telefone"));
                
                fornecedores.add(fornecedor);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao consultar fornecedores!\n"+ ex);
        }finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }
        return fornecedores;
    }
    
    public List<Fornecedor> selecionarFornecedores(String nome){
        con = ConnectionFactory.getConnection();
        List<Fornecedor> fornecedores = new ArrayList<>();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM fornecedor WHERE nome LIKE '"+nome+"%' ORDER BY nome;");
            rs = stmt.executeQuery();
            
            int i=0;
            while(rs.next() && i<8){
                Fornecedor fornecedor = new Fornecedor();
                fornecedor.setCnpj(rs.getString("cnpj"));
                fornecedor.setEmail(rs.getString("email"));
                fornecedor.setNome(rs.getString("nome"));
                fornecedor.setTelefone(rs.getString("telefone"));
                fornecedores.add(fornecedor);
            }
        } catch (SQLException ex) {
            Logger.getLogger(FornecedorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fornecedores;
    }
    
    public int pegarQuantidade(){
        con = ConnectionFactory.getConnection();
        int qtd = 0;
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT COUNT(cnpj) AS total FROM fornecedor;");
            rs = stmt.executeQuery();
            rs.first();
            qtd = rs.getInt("total");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao contar numero de entradas\n"+ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
        return qtd;
    }
}
