/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.dao;

import Connection.ConnectionFactory;
import Model.bean.Estoque;
import Model.bean.Material;
import Model.bean.UnidadeDeMedida;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Jean Ferreira
 */
public class AlertaDAO {
    private Connection con;
    private PreparedStatement stmt;
    private ResultSet rs = null;
    
    private ArrayList<Estoque> dados = null;
    
    public ArrayList<Estoque> quantidadesMinimasAtingidas(){
        con = ConnectionFactory.getConnection();
        dados = new ArrayList<>();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT quantidade, descricao, qtd_minima, tipo FROM estoque INNER JOIN material ON "
                    + "material_idMaterial = idMaterial INNER JOIN unidadeMedida ON material.unidadeMedida_idunidadeMedida = unidadeMedida.idunidadeMedida WHERE quantidade <= qtd_minima ORDER BY quantidade;");
            
            rs = stmt.executeQuery();
            
            while(rs.next()){
                UnidadeDeMedida unidade = new UnidadeDeMedida();
                unidade.setTipo(rs.getString("tipo"));
                Material material = new Material();
                material.setIdUnidadeMedida(unidade);
                material.setDescricao(rs.getString("descricao"));
                material.setQtd_minima(rs.getInt("qtd_minima"));
                Estoque estoque = new Estoque();
                estoque.setQuantidade(rs.getInt("quantidade"));
                estoque.setMaterial_idMaterial(material);
                
                dados.add(estoque);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Não foi possível consultar materiais que atingiram a quantidade mínima.\n"+ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt);
        }
        return dados;
    }
    
    public int quantidadeOcorrencias(){
        con = ConnectionFactory.getConnection();
        
        int q = 0;
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT COUNT(*) AS qtd FROM estoque INNER JOIN material "
                    + "ON estoque.material_idMaterial = material.idMaterial WHERE estoque.quantidade <= material.qtd_minima;");
        
            rs = stmt.executeQuery();
            rs.first();
            
            return q = rs.getInt("qtd");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Não foi possível consultar a quantidade de ocorrencias.\n"+ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt);
        }
        return q;
    }
}
