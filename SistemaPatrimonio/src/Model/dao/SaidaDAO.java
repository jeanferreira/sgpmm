package Model.dao;

import Connection.ConnectionFactory;
import Model.bean.Material;
import Model.bean.Saida;
import Model.bean.Solicitante;
import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class SaidaDAO {
    private Connection con;
    private PreparedStatement stmt;
    private ResultSet rs;
    
    public boolean efetuarSaida(Saida saida){
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("INSERT INTO saida (idSaida, quantidade, dataSaida, solicitante_id, material_idMaterial) VALUES (?,?,?,?,?);");
            stmt.setString(1,null);
            stmt.setInt(2,saida.getQuantidade());
            stmt.setString(3,saida.getData());
            stmt.setInt(4,saida.getSolicitante_id().getId());
            stmt.setInt(5,saida.getMaterial_idMaterial().getIdMaterial());
            
            stmt.executeUpdate();
            return true;
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao salvar saída\n"+ex);
            return false;
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt);
        }
    }
    
    public List<Saida> consultarSaidas(){
        List<Saida> Saida = new ArrayList<>();
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (com.mysql.jdbc.PreparedStatement) con.prepareStatement("SELECT saida.dataSaida, material.descricao, saida.quantidade, "
                    + " solicitante.nome, solicitante.setor FROM saida INNER JOIN material ON saida.material_idMaterial = "
                    + "material.idMaterial LEFT JOIN solicitante ON saida.solicitante_id = solicitante.id ORDER BY saida.dataSaida DESC;");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                Saida saida = new Saida();
                Material material = new Material();
                Solicitante solicitante = new Solicitante();
                
                saida.setData(rs.getString("saida.dataSaida"));
                material.setDescricao(rs.getString("material.descricao"));
                saida.setQuantidade(rs.getInt("saida.quantidade"));
                solicitante.setNome(rs.getString("solicitante.nome"));
                solicitante.setSetor(rs.getString("solicitante.setor"));
                saida.setMaterial_idMaterial(material);
                saida.setSolicitante_id(solicitante);
                Saida.add(saida);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao listar saídas\n"+ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
        return Saida;
    }
    
    public int pegarQuantidade(){
        con = ConnectionFactory.getConnection();
        int qtd = 0;
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT COUNT(*) AS total FROM saida;");
            rs = stmt.executeQuery();
            rs.first();
            qtd = rs.getInt("total");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao contar numero de entradas\n"+ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
        return qtd;
    }
}
