package Model.bean;

public class Entrada {
    private String notaFiscal;
    private String codBarras;
    private int quantidade;
    private String data;
    private TipoAquisicao tipoAquisicaoId;
    private Fornecedor fornecedorCnpj;
    private Material material_idMaterial;

    public String getNotaFiscal() {
        return notaFiscal;
    }

    public void setNotaFiscal(String notaFiscal) {
        this.notaFiscal = notaFiscal;
    }
    
    public String getCodBarras() {
        return codBarras;
    }

    public void setCodBarras(String codBarras) {
        this.codBarras = codBarras;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Material getMaterial_idMaterial() {
        return material_idMaterial;
    }

    public void setMaterial_idMaterial(Material material_idMaterial) {
        this.material_idMaterial = material_idMaterial;
    }

    public Fornecedor getFornecedorCnpj() {
        return fornecedorCnpj;
    }

    public void setFornecedorCnpj(Fornecedor fornecedorCnpj) {
        this.fornecedorCnpj = fornecedorCnpj;
    }

    public TipoAquisicao getTipoAquisicaoId() {
        return tipoAquisicaoId;
    }

    public void setTipoAquisicaoId(TipoAquisicao tipoAquisicaoId) {
        this.tipoAquisicaoId = tipoAquisicaoId;
    }
}