package Model.bean;

public class Estoque {
    private int idEstoque;
    private int quantidade;
    private Entrada entrada_nNotaFiscal;
    private Saida saida_idSaida;
    private Material material_idMaterial;

    public int getIdEstoque() {
        return idEstoque;
    }

    public void setIdEstoque(int idEstoque) {
        this.idEstoque = idEstoque;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Entrada getEntrada_nNotaFiscal() {
        return entrada_nNotaFiscal;
    }

    public void setEntrada_nNotaFiscal(Entrada entrada_nNotaFiscal) {
        this.entrada_nNotaFiscal = entrada_nNotaFiscal;
    }

    public Saida getSaida_idSaida() {
        return saida_idSaida;
    }

    public void setSaida_idSaida(Saida saida_idSaida) {
        this.saida_idSaida = saida_idSaida;
    }

    public Material getMaterial_idMaterial() {
        return material_idMaterial;
    }

    public void setMaterial_idMaterial(Material material_idMaterial) {
        this.material_idMaterial = material_idMaterial;
    }
}