package Model.bean;

public class Saida {
    private int idSaida;
    private int quantidade;
    private String data;
    private Solicitante solicitante_id;
    private Material material_idMaterial;

    public Saida(){}
    
    public Saida(int quantidade, String data, Solicitante solicitante_id, Material material_idMaterial) {
        this.quantidade = quantidade;
        this.data = data;
        this.solicitante_id = solicitante_id;
        this.material_idMaterial = material_idMaterial;
    }

    public int getIdSaida() {
        return idSaida;
    }

    public void setIdSaida(int idSaida) {
        this.idSaida = idSaida;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Solicitante getSolicitante_id() {
        return solicitante_id;
    }

    public void setSolicitante_id(Solicitante solicitante_id) {
        this.solicitante_id = solicitante_id;
    }

    public Material getMaterial_idMaterial() {
        return material_idMaterial;
    }

    public void setMaterial_idMaterial(Material material_idMaterial) {
        this.material_idMaterial = material_idMaterial;
    }
}
