package Model.bean;

public class Solicitante {
    private int id;
    private String nome;
    private String setor;

    public Solicitante() {
    }

    public Solicitante(int id, String nome, String setor) {
        this.id = id;
        this.nome = nome;
        this.setor = setor;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }
    
    @Override
    public String toString(){
        return getNome();
    }
}
