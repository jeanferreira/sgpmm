/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.bean.Entrada;
import Model.dao.EntradaDAO;
import Utils.ModelTable;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Jean
 */
public class TelaVisualizarEntradas extends javax.swing.JFrame {
    private String[] colunas = {"DATA","MATERIAL","QTD","FORNECEDOR","CNPJ","NOTA FISCAL","TIPO DE AQUISIÇÃO"};
    private List<Entrada> entrada = new ArrayList<>();
    private ArrayList dados;
    
    /**
     * Creates new form TelaVisualizarEntradas
     */
    public TelaVisualizarEntradas(TelaPrincipal principal) {
        initComponents();
        inicializarTabela(principal);
    }

    private TelaVisualizarEntradas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void inicializarTabela(TelaPrincipal principal){
        jTableVisualizarEntradas.removeAll();
        
        EntradaDAO entradaDAO = new EntradaDAO();
        jLabelTotal.setText("Quantidade de entradas: "+entradaDAO.pegarQuantidade());
        entrada = entradaDAO.consultarEntradas();
        construirTabela(entrada,principal);
    }
    
    public void construirTabela(List<Entrada> entrada, TelaPrincipal principal){
        dados = new ArrayList();
        
        for(Entrada tabela:entrada){
            dados.add(new Object[]{
                tabela.getData(),
                tabela.getMaterial_idMaterial().getDescricao(),
                tabela.getQuantidade(),
                tabela.getFornecedorCnpj().getNome(),
                tabela.getFornecedorCnpj().getCnpj(),
                tabela.getNotaFiscal(),
                tabela.getTipoAquisicaoId().getDescricao()
            });
        }
        
        ModelTable model = new ModelTable(dados,colunas);
        
        jTableVisualizarEntradas.setModel(model);
        jTableVisualizarEntradas.setAutoResizeMode(jTableVisualizarEntradas.AUTO_RESIZE_ALL_COLUMNS);
        
        jTableVisualizarEntradas.getColumnModel().getColumn(0).setPreferredWidth(30);
        jTableVisualizarEntradas.getColumnModel().getColumn(1).setPreferredWidth(160);
        jTableVisualizarEntradas.getColumnModel().getColumn(2).setPreferredWidth(10);
        jTableVisualizarEntradas.getColumnModel().getColumn(4).setPreferredWidth(80);
        jTableVisualizarEntradas.getColumnModel().getColumn(5).setPreferredWidth(70);
        jTableVisualizarEntradas.getColumnModel().getColumn(6).setPreferredWidth(60);
        jTableVisualizarEntradas.getTableHeader().setResizingAllowed(true);
        jTableVisualizarEntradas.getTableHeader().setReorderingAllowed(false);
        jTableVisualizarEntradas.getSelectionModel().addListSelectionListener(selecionarLinha(principal));
        
        jTableVisualizarEntradas.repaint();
   }
    
    public ListSelectionListener selecionarLinha(TelaPrincipal principal){
        return new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent lse) {
                if(jTableVisualizarEntradas.getSelectedRow() != -1){
                    String notaFiscal = "\""+(String) jTableVisualizarEntradas.getValueAt(jTableVisualizarEntradas.getSelectedRow(), 5)+"\"";
                    
                    EntradaDAO entradaDAO = new EntradaDAO();
                    Entrada entrada = entradaDAO.selecionarEntradaPorNotaFiscal(notaFiscal);
                    Object[] options = {"EDITAR","CANCELAR"};
                    
                    int resposta = JOptionPane.showOptionDialog(getParent(),
                            "Editar entrada de "+entrada.getMaterial_idMaterial().getDescricao()    ,
                            "Deseja enditar?",
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.QUESTION_MESSAGE, null, options, null);
                    
                    if(resposta == 0){
                        
                        principal.requestFocus();
                        principal.irParaEntrada();
                        principal.editarEntrada(entrada);
                        dispose();
                    } else{
                        jTableVisualizarEntradas.clearSelection();
                    }
                }
            }
        };
    }
  
    public void atualizarTabela(TelaPrincipal principal){
        EntradaDAO entradaDAO = new EntradaDAO();
        entrada = entradaDAO.consultarEntradas();
        construirTabela(entrada,principal);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableVisualizarEntradas = new javax.swing.JTable();
        jButtonAtualizar = new javax.swing.JButton();
        jButtonCancelar = new javax.swing.JButton();
        jLabelTotal = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(115, 140, 29), 2));

        jLabel1.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("ENTRADAS DE MATERIAIS");

        jTableVisualizarEntradas.setBackground(new java.awt.Color(115, 140, 29));
        jTableVisualizarEntradas.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jTableVisualizarEntradas.setForeground(new java.awt.Color(255, 255, 255));
        jTableVisualizarEntradas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "DATA", "MATERIAL", "QUANTIDADE", "FORNECEDOR", "CNPJ", "NOTA FISCAL", "TIPO DE AQUISIÇÃO"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableVisualizarEntradas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jTableVisualizarEntradas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableVisualizarEntradasMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableVisualizarEntradas);
        if (jTableVisualizarEntradas.getColumnModel().getColumnCount() > 0) {
            jTableVisualizarEntradas.getColumnModel().getColumn(0).setPreferredWidth(20);
            jTableVisualizarEntradas.getColumnModel().getColumn(2).setPreferredWidth(20);
        }

        jButtonAtualizar.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jButtonAtualizar.setText("ATUALIZAR");
        jButtonAtualizar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonAtualizarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonAtualizarMouseExited(evt);
            }
        });
        jButtonAtualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAtualizarActionPerformed(evt);
            }
        });

        jButtonCancelar.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jButtonCancelar.setText("FECHAR");
        jButtonCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonCancelarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonCancelarMouseExited(evt);
            }
        });
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        jLabelTotal.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabelTotal.setForeground(new java.awt.Color(115, 140, 29));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(98, 98, 98)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonAtualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 962, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(98, 98, 98))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jLabel1)
                .addGap(60, 60, 60)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButtonAtualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 482, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCancelarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonCancelarMouseEntered
        jButtonCancelar.setText("");
        jButtonCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/icons8_delete_sign_32.png")));
        jButtonCancelar.setBackground(Color.RED);
    }//GEN-LAST:event_jButtonCancelarMouseEntered

    private void jButtonCancelarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonCancelarMouseExited
        jButtonCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        jButtonCancelar.setText("CANCELAR");
        jButtonCancelar.setBackground(new Color(240,240,240));
    }//GEN-LAST:event_jButtonCancelarMouseExited

    private void jButtonAtualizarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAtualizarMouseEntered
        jButtonAtualizar.setText("");
        jButtonAtualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/icons8_synchronize_32.png")));
        jButtonAtualizar.setBackground(new Color(115,140,29));
    }//GEN-LAST:event_jButtonAtualizarMouseEntered

    private void jButtonAtualizarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAtualizarMouseExited
        jButtonAtualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        jButtonAtualizar.setText("ATUALIZAR");
        jButtonAtualizar.setBackground(new Color(240,240,240));
    }//GEN-LAST:event_jButtonAtualizarMouseExited

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    private void jButtonAtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAtualizarActionPerformed
        TelaPrincipal p = new TelaPrincipal();
        atualizarTabela(p);
    }//GEN-LAST:event_jButtonAtualizarActionPerformed

    private void jTableVisualizarEntradasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableVisualizarEntradasMouseClicked
        
    }//GEN-LAST:event_jTableVisualizarEntradasMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaVisualizarEntradas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaVisualizarEntradas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaVisualizarEntradas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaVisualizarEntradas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaVisualizarEntradas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAtualizar;
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelTotal;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableVisualizarEntradas;
    // End of variables declaration//GEN-END:variables
}
