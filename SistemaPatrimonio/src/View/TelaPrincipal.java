/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Connection.ConnectionFactory;
import Model.bean.Administrador;
import Model.bean.Categoria;
import Model.bean.Entrada;
import Model.bean.Estoque;
import Model.bean.Fornecedor;
import Model.bean.Material;
import Model.bean.Saida;
import Model.bean.Solicitante;
import Model.bean.TipoAquisicao;
import Model.bean.UnidadeDeMedida;
import Model.dao.AdministradorDAO;
import Model.dao.AlertaDAO;
import Model.dao.EntradaDAO;
import Model.dao.EstoqueDAO;
import Model.dao.FornecedorDAO;
import Model.dao.MaterialDAO;
import Model.dao.SaidaDAO;
import Model.dao.SolicitanteDAO;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mysql.jdbc.PreparedStatement;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import static javax.swing.text.StyleConstants.ALIGN_CENTER;
import Utils.ModelTable;

/**
 *
 * @author Jean Ferreira
 */
public final class TelaPrincipal extends javax.swing.JFrame {
    private Connection con = ConnectionFactory.getConnection();
    private PreparedStatement stmt = null;
    private ResultSet rs = null;

    DefaultListModel MODELOfornecedor;
    DefaultListModel MODELOmaterial;
    DefaultListModel MODELOsolicitante;
    DefaultListModel MODELOdescricao;
    DefaultListModel MODELOrelatorioMaterial;
    
    int Enter = 0;
    int aux = 0;
    boolean modoEditar = false;
    private Boolean alertaVisualizado = false;
    private final String ASPASDUPLAS = "\"";
    private final String ASPASSIMPLES = "\'";
    private final String CONTRABARRAASPASDUPLAS = "\\\"";
    private final String CONTRABARRAASPASSIMPLES = "\\\'";
    
    //Para saída
    private final String[] colunas = {"MATERIAL","QUANTIDADE","SOLICITANTE","DATA"};
    private final ArrayList linhas = new ArrayList<>();
    private final ArrayList<Saida> lista = new ArrayList<>();
    DefaultTableCellRenderer centralizar = new DefaultTableCellRenderer(); //Para configurar célula da tabela
   
    public Administrador adm = new Administrador();
    /**
     * Creates new form TelaPrincipal
     */
    public TelaPrincipal() {
        initComponents();
        verificarAlertas();
        
        URL caminho = getClass().getResource("/imagens/logo/IconeSemBackground.png");
        Image icon = Toolkit.getDefaultToolkit().getImage(caminho);
        this.setIconImage(icon);
        
        //Codigo para pegar ASPASDUPLAS imagem de fundo
        BackgPanel bg;
        bg = new BackgPanel();
        BackgPanelSecundario bs;
        bs = new BackgPanelSecundario();
        jPanelBoxHome.add(bs,BorderLayout.CENTER);

        this.add(bg, BorderLayout.CENTER);

        //Codigo para colocar transparencia nos painéis    
        Transparencia t = new Transparencia();
        t.aplicarTransparencia(jPanel2);
        t.aplicarTransparencia(jPanel4);
        t.aplicarTransparencia(jPanel3);
        t.aplicarTransparencia(jPanel1);
        t.aplicarTransparencia(jPanel7);
        t.aplicarTransparencia(jPanelRelatório);
        t.aplicarTransparencia(jPanelEntrada);
        t.aplicarTransparencia(jPanelHome);
        //fim código     

        listarComboBoxAquisicao();
        listarComboBoxSetor();
        
        jListFornecedor.setVisible(false);
        jListMaterial.setVisible(false);
        jListSolicitante.setVisible(false);
        jListDescricao.setVisible(false);
        jListRelatorioMaterial.setVisible(false);

        MODELOfornecedor = new DefaultListModel();
        jListFornecedor.setModel(MODELOfornecedor);
        MODELOmaterial = new DefaultListModel();
        jListMaterial.setModel(MODELOmaterial);
        MODELOsolicitante = new DefaultListModel();
        jListSolicitante.setModel(MODELOsolicitante);
        MODELOdescricao = new DefaultListModel();
        jListDescricao.setModel(MODELOdescricao);
        MODELOrelatorioMaterial = new DefaultListModel();
        jListRelatorioMaterial.setModel(MODELOrelatorioMaterial);

        Date data = new Date();
        String dataFormatada = new SimpleDateFormat("dd/MM/yyyy").format(/*contato.getDataNascimento()*/data);// ("dd/MM/yyyy HH:mm:ss")
        jFormattedTextData.setText(dataFormatada);
        
        String dataSaida = new SimpleDateFormat("dd/MM/yyyy").format(data);
        jFormattedDataSaida.setText(dataSaida);
        
        this.setExtendedState(MAXIMIZED_BOTH); //Comando para abrir janela maximizada
        
        //Preenche os campos do relatório
        jFormattedDataInicio.setText(dataSaida);
        jFormattedDataFinal.setText(dataSaida);
        jFormattedDataInicioMaterial.setText(dataSaida);
        jFormattedDataFinalMaterial.setText(dataSaida);
        jFormattedDataInicioSetor.setText(dataSaida);
        jFormattedDataFinalSetor.setText(dataSaida);
        jCheckBoxEntrada.setSelected(true);
        jCheckBoxSaida.setSelected(true);
        jCheckBoxEntradaMaterial.setSelected(true);
        jCheckBoxSaidaMaterial.setSelected(true);
        jCheckBoxDataMaterial.setSelected(true);
        jCheckBoxDataSetor.setSelected(true);
        jLabelEditarEntrada.setVisible(false);
        jButtonCancelarEditar.setVisible(false);
        
        jSpinnerQuantidade.setEnabled(false);
        jButtonCadastrar.setEnabled(false);
        jSpinnerQuantidadeRetirada.setEnabled(false);
        jButtonAdicionar.setEnabled(false);
        jButtonSaida.setEnabled(false);
        jTextFieldUnidadeMedida.setEditable(false);
        
        
        centralizar.setHorizontalAlignment(SwingConstants.CENTER); //Define celula como centralizada
        
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); //Confirma se o usuário deseja fechar a janela principal
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt){
                Object[] options = {"Confirmar","Cancelar"};
                int resposta = JOptionPane.showOptionDialog(getParent(),
                            "Deseja realmente sair?",
                            "AVISO",
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.QUESTION_MESSAGE,null,options,null);
                if(resposta==0){
                    System.exit(0);
                    ConnectionFactory.closeConnection(con,stmt,rs);
                }
            }        
        });
    }
    
    public void setAdministrador(Administrador adm){
        AdministradorDAO admDAO = new AdministradorDAO();
        this.adm = admDAO.selecionarAdministrador(adm);
        jLabelNomeAdministrador.setText(this.adm.getNome());
    }
    
    public boolean camposSaidaVazios(){
        if(jTextDescricao.getText().trim().isEmpty() || jFormattedDataSaida.getText().trim().equals("/  /") || Integer.parseInt(jSpinnerQuantidadeRetirada.getValue().toString()) <= 0)
            return true;
        return false;
    }
    
    public void bloquearCamposSaida(){
        if(jSpinnerQuantidadeRetirada.isEnabled() || jButtonAdicionar.isEnabled() || jButtonSaida.isEnabled()){
            jSpinnerQuantidadeRetirada.setEnabled(false);
            jButtonAdicionar.setEnabled(false);
            jButtonSaida.setEnabled(false);
        }
    }
    
    public String formatarData(String data){
        String[] dataSeparada = data.split("/");
        LocalDate dataFormatada = LocalDate.of(Integer.parseInt(dataSeparada[2]),Integer.parseInt(dataSeparada[1]),Integer.parseInt(dataSeparada[0]));
        return dataFormatada.toString();
    }
    
    public void verificarAlertas(){
        AlertaDAO alerta = new AlertaDAO();
        int qtd = alerta.quantidadeOcorrencias();
        
        if(qtd > 0 && alertaVisualizado == false){
            modoAlerta(qtd);
        } else{
            jLabelQtdAlertas.setText("");
            jButtonAlerta.setBackground(new Color(240,240,240));
            jButtonAlerta.setForeground(Color.BLACK);
        }
    }
    
    public void modoAlerta(int qtd){
        jLabelQtdAlertas.setText(Integer.toString(qtd));
        jLabelQtdAlertas.setForeground(new Color(255,153,0));
        jButtonAlerta.setBackground(new Color(255,153,0));
        jButtonAlerta.setForeground(new Color(255,153,0));
        jLabelAlertas.setForeground(new Color(255,153,0));
    }

    public void listarComboBoxAquisicao() {
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT descricao FROM tipoaquisicao ORDER BY descricao;");
            rs = stmt.executeQuery();

            while (rs.next()) {
                jComboBoxAquisicao.addItem(rs.getString("descricao"));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar os tipos de aquisição!" + ex);
        } finally {
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
    }
    
    public void listarComboBoxSetor(){
        con = ConnectionFactory.getConnection();
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT DISTINCT setor FROM solicitante;");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                jComboBoxSetor.addItem(rs.getString("setor"));
            }
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao listar setores"+ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
    }

    public void listaPesquisaFornecedor(String nome) {
        MODELOfornecedor.clear();
        List<Fornecedor> fornecedores;
        FornecedorDAO fDAO = new FornecedorDAO();
        
        fornecedores = fDAO.selecionarFornecedores(nome);
        
        jListFornecedor.setVisible(true);
        fornecedores.forEach((f) ->{
            MODELOfornecedor.addElement((Fornecedor)f);
        });
    }

    public void listaPesquisaMaterial(String desc, DefaultListModel model, JList jList) {
        model.clear();
        List<Material> materiais;
        MaterialDAO mDAO = new MaterialDAO();
        
        materiais = mDAO.selecionarMateriais(desc);
        
        jList.setVisible(true);
        materiais.forEach((m) -> {
            model.addElement(m);
        });
    }
    
    //Pesquisa da saída

    public void listaPesquisaNome(String nome) {
        MODELOsolicitante.clear();
        SolicitanteDAO sDAO = new SolicitanteDAO();
        List<Solicitante> solicitantes;
        solicitantes = sDAO.selecionarSolicitantes(nome);
        
        jListSolicitante.setVisible(true);
        solicitantes.forEach((s) -> {
            MODELOsolicitante.addElement(s);
        });
    }
    
    public void listaPesquisaRelatorioMaterial() {
        con = ConnectionFactory.getConnection();
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM material "
                    + "WHERE descricao like '" + jTextRelatorioMaterial.getText() + "%' ORDER BY descricao");
            rs = stmt.executeQuery();

            MODELOrelatorioMaterial.removeAllElements();
            int v = 0;
            while (rs.next() & v < 4) {
                MODELOrelatorioMaterial.addElement(rs.getString("descricao"));
                v++;
            }
            if (v >= 1) {
                jListRelatorioMaterial.setVisible(true);
            } else {
                jListRelatorioMaterial.setVisible(false);
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao listar dricrições\n" + ex);
        } finally {
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
    }
    
    public void addTabela(Saida saida){
        linhas.add(new Object[]{
            saida.getMaterial_idMaterial(),
            saida.getQuantidade(),
            saida.getSolicitante_id(),
            saida.getData()
        });

        ModelTable m = new ModelTable(linhas, colunas);
        jTableSaidaMaterial.setModel(m);
        //jTableSaidaMaterial.getColumn(1).setCellRenderer(centralizar);
        //jTableSaidaMaterial.getColumn(3).setCellRenderer(centralizar);
        jTableSaidaMaterial.getSelectionModel().addListSelectionListener(selecionarLinha());
        jTableSaidaMaterial.repaint();
    }
    
    public ListSelectionListener selecionarLinha(){
        return new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent lse) {
                if(jTableSaidaMaterial.getSelectedRow() != -1){
                    
                    Object[] options = {"NÃO","SIM"};
                    
                    int resposta = JOptionPane.showOptionDialog(getParent(),
                            "Deseja remover?",
                            "ALERTA",
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.QUESTION_MESSAGE, null, options, null);
                    
                    if(resposta == 1){
                        linhas.remove(jTableSaidaMaterial.getSelectedRow());
                        lista.remove(jTableSaidaMaterial.getSelectedRow());
                        jTableSaidaMaterial.updateUI();
                        JOptionPane.showMessageDialog(getParent(),"Item removido com sucesso!","CONFIRMADO",JOptionPane.PLAIN_MESSAGE);
                        verificarQuantidadeTotalDisponivel(); //Atualiza a quantidade que o usuario pode retirar
                    }
                    jTableSaidaMaterial.clearSelection();
                }
            }
        };
    }
    
    private int somarQuantidadeEmTabela(){
        int i = 0;
        if(!lista.isEmpty()){
            for(Saida s : lista){
                if(s.getMaterial_idMaterial().getDescricao().equals(jTextDescricao.getText().trim()))
                    i+=s.getQuantidade();
            }
        }
        return i;
    }
    
    private void verificarQuantidadeTotalDisponivel(){
        EstoqueDAO eDAO = new EstoqueDAO();
                
        if(eDAO.quantidadeSolicitada(jTextDescricao.getText().trim(),Integer.parseInt(jSpinnerQuantidadeRetirada.getValue().toString()), somarQuantidadeEmTabela())){
            if(!jButtonAdicionar.isEnabled())
                jButtonAdicionar.setEnabled(true);
            
            if(!lista.isEmpty()){
                if(!jButtonSaida.isEnabled())
                    jButtonSaida.setEnabled(true);
            } else
                jButtonSaida.setEnabled(false);
        } else{
            jButtonAdicionar.setEnabled(false);
            JOptionPane.showMessageDialog(null,"Quantidade execede a quantidade em estoque.");
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        buttonGroup1 = new javax.swing.ButtonGroup();
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        jTabbedPrincipal = new javax.swing.JTabbedPane();
        jPanelHome = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jPanelBoxHome = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jButtonEstoqueVisualizar = new javax.swing.JButton();
        jLabelSolicitantes = new javax.swing.JLabel();
        jButtonSolicitantesMais = new javax.swing.JButton();
        jButtonSolicitantesVisualizar = new javax.swing.JButton();
        jLabelEntradas = new javax.swing.JLabel();
        jButtonEntradasVisualizar = new javax.swing.JButton();
        jLabelSaidas = new javax.swing.JLabel();
        jLabelFornecedor = new javax.swing.JLabel();
        jButtonFornecedorMais = new javax.swing.JButton();
        jButtonFornecedorVisualizar = new javax.swing.JButton();
        jLabelEstoque = new javax.swing.JLabel();
        jButtonSaidasVisualizar = new javax.swing.JButton();
        jLabelMaterialMais = new javax.swing.JLabel();
        jButtonMaterialMais = new javax.swing.JButton();
        jButtonEntradaMais = new javax.swing.JButton();
        jButtonSaidaMais = new javax.swing.JButton();
        jButtonAlerta = new javax.swing.JButton();
        jLabelAlertas = new javax.swing.JLabel();
        jLabelQtdAlertas = new javax.swing.JLabel();
        jLabelCategorias = new javax.swing.JLabel();
        jButtonAdicionarUnidadeMedida = new javax.swing.JButton();
        jButtonPesquisaUnidadeMedida = new javax.swing.JButton();
        jLabelUnidadeMedida = new javax.swing.JLabel();
        jButtonAdicionarCategoria = new javax.swing.JButton();
        jButtonPesquisaCategorias = new javax.swing.JButton();
        jButtonPesquisaMateriais = new javax.swing.JButton();
        jButtonGerarRelatorio = new javax.swing.JButton();
        jLabelGerarRelatorios = new javax.swing.JLabel();
        jLabelNomeAdministrador = new javax.swing.JLabel();
        jPanelEntrada = new javax.swing.JPanel();
        jPanelBoxEntrada = new javax.swing.JPanel();
        jFormattedTextData = new javax.swing.JFormattedTextField();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableCadastroMaterial = new javax.swing.JTable();
        jSpinnerQuantidade = new javax.swing.JSpinner();
        jLabel12 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jListMaterial = new javax.swing.JList<>();
        jTextFornecedor = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextCodigoDeBarras = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jListFornecedor = new javax.swing.JList<>();
        jLabel5 = new javax.swing.JLabel();
        jComboBoxAquisicao = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jButtonAdicionarMaterial = new javax.swing.JButton();
        jTextMaterial = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTextNotaFiscal = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jButtonAdicionarFornecedor = new javax.swing.JButton();
        jButtonCadastrar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabelEditarEntrada = new javax.swing.JLabel();
        jButtonCancelarEditar = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        jLabelTituloEntrada = new javax.swing.JLabel();
        jLayeredPaneSaida = new javax.swing.JLayeredPane();
        jPanelBoxSaida = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTableSaidaMaterial = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jListSolicitante = new javax.swing.JList<>();
        jTextSolicitante = new javax.swing.JTextField();
        jButtonAdicionarSolicitante = new javax.swing.JButton();
        jFormattedDataSaida = new javax.swing.JFormattedTextField();
        jLabel13 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jTextDescricao = new javax.swing.JTextField();
        jListDescricao = new javax.swing.JList<>();
        jLabel26 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jTextFieldUnidadeMedida = new javax.swing.JTextField();
        jButtonSaida = new javax.swing.JButton();
        jSpinnerQuantidadeRetirada = new javax.swing.JSpinner();
        jLabel20 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jButtonAdicionar = new javax.swing.JButton();
        jLabel28 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jPanelRelatório = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jPanelBoxRelatorio = new javax.swing.JPanel();
        jPanelRelatorio = new javax.swing.JPanel();
        jButtonRelatorio = new javax.swing.JButton();
        jCheckBoxEntrada = new javax.swing.JCheckBox();
        jCheckBoxSaida = new javax.swing.JCheckBox();
        jFormattedDataInicio = new javax.swing.JFormattedTextField();
        jFormattedDataFinal = new javax.swing.JFormattedTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jButtonEmitirMaterial = new javax.swing.JButton();
        jTextRelatorioMaterial = new javax.swing.JTextField();
        jListRelatorioMaterial = new javax.swing.JList<>();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel22 = new javax.swing.JLabel();
        jButtonEmitirSetor = new javax.swing.JButton();
        jComboBoxSetor = new javax.swing.JComboBox<>();
        jCheckBoxEntradaMaterial = new javax.swing.JCheckBox();
        jCheckBoxSaidaMaterial = new javax.swing.JCheckBox();
        jRadioButtonTodos = new javax.swing.JRadioButton();
        jFormattedDataInicioMaterial = new javax.swing.JFormattedTextField();
        jLabel29 = new javax.swing.JLabel();
        jFormattedDataFinalMaterial = new javax.swing.JFormattedTextField();
        jFormattedDataFinalSetor = new javax.swing.JFormattedTextField();
        jFormattedDataInicioSetor = new javax.swing.JFormattedTextField();
        jLabel31 = new javax.swing.JLabel();
        jCheckBoxDataSetor = new javax.swing.JCheckBox();
        jCheckBoxDataMaterial = new javax.swing.JCheckBox();
        filler3 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenuConfiguracoes = new javax.swing.JMenu();
        jMenuItemAdministrador = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();

        jMenu1.setText("jMenu1");

        jMenu2.setText("jMenu2");

        jLabel3.setText("jLabel3");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sistema de Gerênciamento de Estoque, Gerência de Patrimônio, Materiais e Manutenção");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jTabbedPrincipal.setToolTipText("");
        jTabbedPrincipal.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jTabbedPrincipal.setPreferredSize(new java.awt.Dimension(1113, 765));

        jPanelHome.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanelHome.setOpaque(false);
        jPanelHome.setPreferredSize(new java.awt.Dimension(1113, 765));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/Logo_Dimensões/398x200/1.png"))); // NOI18N

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/logo/Imagem1.png"))); // NOI18N

        jLabel24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/logo/logo icet branca.png"))); // NOI18N

        jPanelBoxHome.setPreferredSize(new java.awt.Dimension(1112, 218));

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "CADASTROS E VISUALIZAÇÕES", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Century Gothic", 1, 18))); // NOI18N
        jPanel5.setForeground(new java.awt.Color(255, 255, 255));
        jPanel5.setFocusable(false);
        jPanel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jPanel5.setOpaque(false);
        jPanel5.setVerifyInputWhenFocusTarget(false);

        jButtonEstoqueVisualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/seo-social-web-network-internet_340_icon-icons.com_61497.png"))); // NOI18N
        jButtonEstoqueVisualizar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonEstoqueVisualizarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonEstoqueVisualizarMouseExited(evt);
            }
        });
        jButtonEstoqueVisualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEstoqueVisualizarActionPerformed(evt);
            }
        });

        jLabelSolicitantes.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabelSolicitantes.setText("Solicitantes");

        jButtonSolicitantesMais.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/ic-plus_97608.png"))); // NOI18N
        jButtonSolicitantesMais.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonSolicitantesMaisMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonSolicitantesMaisMouseExited(evt);
            }
        });
        jButtonSolicitantesMais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSolicitantesMaisActionPerformed(evt);
            }
        });

        jButtonSolicitantesVisualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/seo-social-web-network-internet_340_icon-icons.com_61497.png"))); // NOI18N
        jButtonSolicitantesVisualizar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonSolicitantesVisualizarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonSolicitantesVisualizarMouseExited(evt);
            }
        });
        jButtonSolicitantesVisualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSolicitantesVisualizarActionPerformed(evt);
            }
        });

        jLabelEntradas.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabelEntradas.setText("Entradas");

        jButtonEntradasVisualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/seo-social-web-network-internet_340_icon-icons.com_61497.png"))); // NOI18N
        jButtonEntradasVisualizar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonEntradasVisualizarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonEntradasVisualizarMouseExited(evt);
            }
        });
        jButtonEntradasVisualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEntradasVisualizarActionPerformed(evt);
            }
        });

        jLabelSaidas.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabelSaidas.setText("Saídas");

        jLabelFornecedor.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabelFornecedor.setText("Fornecedores");

        jButtonFornecedorMais.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/ic-plus_97608.png"))); // NOI18N
        jButtonFornecedorMais.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonFornecedorMaisMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonFornecedorMaisMouseExited(evt);
            }
        });
        jButtonFornecedorMais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonFornecedorMaisActionPerformed(evt);
            }
        });

        jButtonFornecedorVisualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/seo-social-web-network-internet_340_icon-icons.com_61497.png"))); // NOI18N
        jButtonFornecedorVisualizar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonFornecedorVisualizarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonFornecedorVisualizarMouseExited(evt);
            }
        });
        jButtonFornecedorVisualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonFornecedorVisualizarActionPerformed(evt);
            }
        });

        jLabelEstoque.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabelEstoque.setText("Estoque");

        jButtonSaidasVisualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/seo-social-web-network-internet_340_icon-icons.com_61497.png"))); // NOI18N
        jButtonSaidasVisualizar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonSaidasVisualizarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonSaidasVisualizarMouseExited(evt);
            }
        });
        jButtonSaidasVisualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaidasVisualizarActionPerformed(evt);
            }
        });

        jLabelMaterialMais.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabelMaterialMais.setText("Materiais");

        jButtonMaterialMais.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/ic-plus_97608.png"))); // NOI18N
        jButtonMaterialMais.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonMaterialMaisMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonMaterialMaisMouseExited(evt);
            }
        });
        jButtonMaterialMais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonMaterialMaisActionPerformed(evt);
            }
        });

        jButtonEntradaMais.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/ic-plus_97608.png"))); // NOI18N
        jButtonEntradaMais.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonEntradaMaisMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonEntradaMaisMouseExited(evt);
            }
        });
        jButtonEntradaMais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEntradaMaisActionPerformed(evt);
            }
        });

        jButtonSaidaMais.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/ic-plus_97608.png"))); // NOI18N
        jButtonSaidaMais.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonSaidaMaisMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonSaidaMaisMouseExited(evt);
            }
        });
        jButtonSaidaMais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaidaMaisActionPerformed(evt);
            }
        });

        jButtonAlerta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/ios7-bell-outline_icon-icons.com_50335.png"))); // NOI18N
        jButtonAlerta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonAlertaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonAlertaMouseExited(evt);
            }
        });
        jButtonAlerta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAlertaActionPerformed(evt);
            }
        });

        jLabelAlertas.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabelAlertas.setText("Alertas");

        jLabelQtdAlertas.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabelQtdAlertas.setForeground(new java.awt.Color(255, 0, 0));
        jLabelQtdAlertas.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        jLabelCategorias.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        jLabelCategorias.setText("Categorias");

        jButtonAdicionarUnidadeMedida.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/ic-plus_97608.png"))); // NOI18N
        jButtonAdicionarUnidadeMedida.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonAdicionarUnidadeMedidaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonAdicionarUnidadeMedidaMouseExited(evt);
            }
        });
        jButtonAdicionarUnidadeMedida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarUnidadeMedidaActionPerformed(evt);
            }
        });

        jButtonPesquisaUnidadeMedida.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/seo-social-web-network-internet_340_icon-icons.com_61497.png"))); // NOI18N
        jButtonPesquisaUnidadeMedida.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonPesquisaUnidadeMedidaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonPesquisaUnidadeMedidaMouseExited(evt);
            }
        });
        jButtonPesquisaUnidadeMedida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPesquisaUnidadeMedidaActionPerformed(evt);
            }
        });

        jLabelUnidadeMedida.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        jLabelUnidadeMedida.setText("Unidades de Medida");

        jButtonAdicionarCategoria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/ic-plus_97608.png"))); // NOI18N
        jButtonAdicionarCategoria.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonAdicionarCategoriaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonAdicionarCategoriaMouseExited(evt);
            }
        });
        jButtonAdicionarCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarCategoriaActionPerformed(evt);
            }
        });

        jButtonPesquisaCategorias.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/seo-social-web-network-internet_340_icon-icons.com_61497.png"))); // NOI18N
        jButtonPesquisaCategorias.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonPesquisaCategoriasMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonPesquisaCategoriasMouseExited(evt);
            }
        });
        jButtonPesquisaCategorias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPesquisaCategoriasActionPerformed(evt);
            }
        });

        jButtonPesquisaMateriais.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/seo-social-web-network-internet_340_icon-icons.com_61497.png"))); // NOI18N
        jButtonPesquisaMateriais.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonPesquisaMateriaisMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonPesquisaMateriaisMouseExited(evt);
            }
        });
        jButtonPesquisaMateriais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPesquisaMateriaisActionPerformed(evt);
            }
        });

        jButtonGerarRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/pdf-2_balck_26px.png"))); // NOI18N
        jButtonGerarRelatorio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonGerarRelatorioMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonGerarRelatorioMouseExited(evt);
            }
        });
        jButtonGerarRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGerarRelatorioActionPerformed(evt);
            }
        });

        jLabelGerarRelatorios.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        jLabelGerarRelatorios.setText("Gerar Relatórios");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(123, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabelAlertas, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonAlerta, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabelQtdAlertas, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabelEstoque)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonEstoqueVisualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelFornecedor)
                    .addComponent(jLabelSolicitantes))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jButtonFornecedorMais, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonFornecedorVisualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jButtonSolicitantesMais, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonSolicitantesVisualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabelEntradas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabelSaidas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jButtonEntradaMais, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonEntradasVisualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jButtonSaidaMais, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonSaidasVisualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 62, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabelCategorias, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonAdicionarCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabelUnidadeMedida)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonAdicionarUnidadeMedida, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonPesquisaCategorias, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonPesquisaUnidadeMedida, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 61, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabelMaterialMais)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonMaterialMais, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabelGerarRelatorios))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButtonPesquisaMateriais, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonGerarRelatorio, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap(123, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jButtonMaterialMais, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelMaterialMais, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelCategorias, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonPesquisaCategorias, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonAdicionarCategoria, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonPesquisaMateriais))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabelEntradas, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonFornecedorVisualizar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonFornecedorMais, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabelEstoque, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonEstoqueVisualizar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE))
                    .addComponent(jButtonEntradaMais, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonEntradasVisualizar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabelQtdAlertas, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabelGerarRelatorios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonSolicitantesMais, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelAlertas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelSaidas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonSaidaMais, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonSaidasVisualizar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelUnidadeMedida, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonSolicitantesVisualizar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelSolicitantes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonPesquisaUnidadeMedida, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
                        .addComponent(jButtonAdicionarUnidadeMedida, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonGerarRelatorio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonAlerta, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(43, 43, 43))
        );

        javax.swing.GroupLayout jPanelBoxHomeLayout = new javax.swing.GroupLayout(jPanelBoxHome);
        jPanelBoxHome.setLayout(jPanelBoxHomeLayout);
        jPanelBoxHomeLayout.setHorizontalGroup(
            jPanelBoxHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBoxHomeLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(20, 20, 20))
        );
        jPanelBoxHomeLayout.setVerticalGroup(
            jPanelBoxHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBoxHomeLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );

        jLabelNomeAdministrador.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        jLabelNomeAdministrador.setForeground(new java.awt.Color(255, 255, 255));
        jLabelNomeAdministrador.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelNomeAdministrador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/icons8_user_32_1.png"))); // NOI18N
        jLabelNomeAdministrador.setText("Administrador");

        javax.swing.GroupLayout jPanelHomeLayout = new javax.swing.GroupLayout(jPanelHome);
        jPanelHome.setLayout(jPanelHomeLayout);
        jPanelHomeLayout.setHorizontalGroup(
            jPanelHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelHomeLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel24)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel16)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanelHomeLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanelBoxHome, javax.swing.GroupLayout.DEFAULT_SIZE, 1462, Short.MAX_VALUE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jLabelNomeAdministrador, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanelHomeLayout.setVerticalGroup(
            jPanelHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelHomeLayout.createSequentialGroup()
                .addGap(69, 69, 69)
                .addGroup(jPanelHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel24)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(49, 49, 49)
                .addComponent(jPanelBoxHome, javax.swing.GroupLayout.PREFERRED_SIZE, 299, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 361, Short.MAX_VALUE)
                .addComponent(jLabelNomeAdministrador)
                .addGap(60, 60, 60))
        );

        jTabbedPrincipal.addTab("Home", jPanelHome);

        jPanelEntrada.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanelEntrada.setOpaque(false);

        try {
            jFormattedTextData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedTextData.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jFormattedTextData.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jFormattedTextData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFormattedTextDataActionPerformed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1), "ADICIONADOS RECENTEMENTE", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Century Gothic", 1, 18))); // NOI18N
        jPanel4.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jPanel4.setOpaque(false);

        jTableCadastroMaterial.setBackground(new java.awt.Color(115, 140, 29));
        jTableCadastroMaterial.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jTableCadastroMaterial.setForeground(new java.awt.Color(255, 255, 255));
        jTableCadastroMaterial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NOTA FISCAL", "DESCRIÇÃO", "QUANTIDADE", "DATA", "AQUISIÇÃO", "FORNECEDOR"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTableCadastroMaterial);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jScrollPane2))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jSpinnerQuantidade.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jSpinnerQuantidade.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSpinnerQuantidadeStateChanged(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel12.setText("Quantidade");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "INFORMAÇÕES DO LOTE", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Century Gothic", 1, 18))); // NOI18N
        jPanel2.setOpaque(false);

        jListMaterial.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jListMaterial.setFocusable(false);
        jListMaterial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListMaterialMouseClicked(evt);
            }
        });

        jTextFornecedor.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jTextFornecedor.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextFornecedorFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFornecedorFocusLost(evt);
            }
        });
        jTextFornecedor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTextFornecedorMousePressed(evt);
            }
        });
        jTextFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFornecedorActionPerformed(evt);
            }
        });
        jTextFornecedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFornecedorKeyReleased(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel2.setText("Código de Barras");

        jTextCodigoDeBarras.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jTextCodigoDeBarras.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextCodigoDeBarrasFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextCodigoDeBarrasFocusLost(evt);
            }
        });
        jTextCodigoDeBarras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextCodigoDeBarrasActionPerformed(evt);
            }
        });
        jTextCodigoDeBarras.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextCodigoDeBarrasKeyReleased(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel4.setText("Fornecedor");

        jListFornecedor.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jListFornecedor.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jListFornecedor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListFornecedorMouseClicked(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel5.setText("Tipo de Aquisição");

        jComboBoxAquisicao.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jComboBoxAquisicao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxAquisicaoActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel1.setText("Material");

        jButtonAdicionarMaterial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/plus_math_black_26px.png"))); // NOI18N
        jButtonAdicionarMaterial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonAdicionarMaterialMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonAdicionarMaterialMouseExited(evt);
            }
        });
        jButtonAdicionarMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarMaterialActionPerformed(evt);
            }
        });

        jTextMaterial.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jTextMaterial.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextMaterialFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextMaterialFocusLost(evt);
            }
        });
        jTextMaterial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTextMaterialMousePressed(evt);
            }
        });
        jTextMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextMaterialActionPerformed(evt);
            }
        });
        jTextMaterial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextMaterialKeyReleased(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel7.setText("Nota Fiscal");

        jTextNotaFiscal.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jTextNotaFiscal.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextNotaFiscalFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextNotaFiscalFocusLost(evt);
            }
        });
        jTextNotaFiscal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextNotaFiscalActionPerformed(evt);
            }
        });
        jTextNotaFiscal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextNotaFiscalKeyReleased(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 0, 0));
        jLabel8.setText("*");

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 0, 0));
        jLabel15.setText("*");

        jButtonAdicionarFornecedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/plus_math_black_26px.png"))); // NOI18N
        jButtonAdicionarFornecedor.setOpaque(false);
        jButtonAdicionarFornecedor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonAdicionarFornecedorMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonAdicionarFornecedorMouseExited(evt);
            }
        });
        jButtonAdicionarFornecedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarFornecedorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jListMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 608, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 608, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonAdicionarMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jTextNotaFiscal, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jListFornecedor, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jTextFornecedor, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
                            .addComponent(jTextCodigoDeBarras, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonAdicionarFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(48, 48, 48)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jComboBoxAquisicao, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(41, 41, 41)))))
                .addGap(28, 28, 28))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonAdicionarMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jTextMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel15)))
                .addComponent(jListMaterial, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(22, 22, 22)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jTextNotaFiscal, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(jComboBoxAquisicao, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(20, 20, 20)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextCodigoDeBarras, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonAdicionarFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(jTextFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addComponent(jListFornecedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButtonCadastrar.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        jButtonCadastrar.setText("SALVAR");
        jButtonCadastrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonCadastrarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonCadastrarMouseExited(evt);
            }
        });
        jButtonCadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCadastrarActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel6.setText("Data");

        jLabel25.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(255, 0, 0));
        jLabel25.setText("*");

        jLabelEditarEntrada.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabelEditarEntrada.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/editar.png"))); // NOI18N
        jLabelEditarEntrada.setText("MODO EDIÇÃO");

        jButtonCancelarEditar.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        jButtonCancelarEditar.setText("CANCELAR");
        jButtonCancelarEditar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonCancelarEditarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonCancelarEditarMouseExited(evt);
            }
        });
        jButtonCancelarEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarEditarActionPerformed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 0, 0));
        jLabel21.setText("*");

        javax.swing.GroupLayout jPanelBoxEntradaLayout = new javax.swing.GroupLayout(jPanelBoxEntrada);
        jPanelBoxEntrada.setLayout(jPanelBoxEntradaLayout);
        jPanelBoxEntradaLayout.setHorizontalGroup(
            jPanelBoxEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBoxEntradaLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(jPanelBoxEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanelBoxEntradaLayout.createSequentialGroup()
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSpinnerQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonCancelarEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonCadastrar, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelBoxEntradaLayout.createSequentialGroup()
                        .addComponent(jLabelEditarEntrada)
                        .addGap(188, 188, 188)
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6)
                        .addGap(18, 18, 18)
                        .addComponent(jFormattedTextData, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(50, 50, 50))
        );
        jPanelBoxEntradaLayout.setVerticalGroup(
            jPanelBoxEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBoxEntradaLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanelBoxEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jFormattedTextData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jLabelEditarEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(jPanelBoxEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBoxEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButtonCadastrar, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButtonCancelarEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel25, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanelBoxEntradaLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(jPanelBoxEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jSpinnerQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(30, 30, 30)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40))
        );

        jLabelTituloEntrada.setBackground(new java.awt.Color(115, 140, 29));
        jLabelTituloEntrada.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        jLabelTituloEntrada.setForeground(new java.awt.Color(255, 255, 255));
        jLabelTituloEntrada.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelTituloEntrada.setText("ENTRADA DE MATERIAL DE CONSUMO");
        jLabelTituloEntrada.setOpaque(true);

        javax.swing.GroupLayout jPanelEntradaLayout = new javax.swing.GroupLayout(jPanelEntrada);
        jPanelEntrada.setLayout(jPanelEntradaLayout);
        jPanelEntradaLayout.setHorizontalGroup(
            jPanelEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelEntradaLayout.createSequentialGroup()
                .addContainerGap(269, Short.MAX_VALUE)
                .addGroup(jPanelEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanelBoxEntrada, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabelTituloEntrada, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(268, Short.MAX_VALUE))
        );
        jPanelEntradaLayout.setVerticalGroup(
            jPanelEntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelEntradaLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(jLabelTituloEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanelBoxEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPrincipal.addTab("Entrada", jPanelEntrada);
        jPanelEntrada.getAccessibleContext().setAccessibleDescription("");

        jPanelBoxSaida.setPreferredSize(new java.awt.Dimension(949, 806));

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1), "LISTA DE SAÍDA DE MATERIAIS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Century Gothic", 1, 18))); // NOI18N
        jPanel7.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jPanel7.setOpaque(false);
        jPanel7.setPreferredSize(new java.awt.Dimension(462, 242));

        jTableSaidaMaterial.setBackground(new java.awt.Color(115, 140, 29));
        jTableSaidaMaterial.setForeground(new java.awt.Color(255, 255, 255));
        jTableSaidaMaterial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "MATERIAL", "QUANTIDADE", "SOLICITANTE", "DATA"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class, java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(jTableSaidaMaterial);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jScrollPane4)
                .addGap(0, 0, 0))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "BUSCAR POR SOLICITANTE", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Century Gothic", 1, 18))); // NOI18N
        jPanel3.setOpaque(false);
        jPanel3.setPreferredSize(new java.awt.Dimension(849, 287));

        jLabel11.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel11.setText("Nome");

        jListSolicitante.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jListSolicitante.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListSolicitanteMouseClicked(evt);
            }
        });

        jTextSolicitante.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jTextSolicitante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextSolicitanteActionPerformed(evt);
            }
        });
        jTextSolicitante.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextSolicitanteKeyReleased(evt);
            }
        });

        jButtonAdicionarSolicitante.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/plus_math_black_26px.png"))); // NOI18N
        jButtonAdicionarSolicitante.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonAdicionarSolicitanteMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonAdicionarSolicitanteMouseExited(evt);
            }
        });
        jButtonAdicionarSolicitante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarSolicitanteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jLabel11)
                .addGap(44, 44, 44)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jListSolicitante, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextSolicitante, javax.swing.GroupLayout.PREFERRED_SIZE, 463, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonAdicionarSolicitante, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(214, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextSolicitante, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonAdicionarSolicitante, javax.swing.GroupLayout.PREFERRED_SIZE, 34, Short.MAX_VALUE))
                .addGap(0, 0, 0)
                .addComponent(jListSolicitante, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );

        try {
            jFormattedDataSaida.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedDataSaida.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jFormattedDataSaida.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N

        jLabel13.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel13.setText("Data");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "BUSCAR POR MATERIAL", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Century Gothic", 1, 18))); // NOI18N
        jPanel1.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jPanel1.setOpaque(false);

        jLabel9.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel9.setText("Descrição");

        jTextDescricao.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jTextDescricao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextDescricaoActionPerformed(evt);
            }
        });
        jTextDescricao.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextDescricaoKeyReleased(evt);
            }
        });

        jListDescricao.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jListDescricao.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListDescricaoMouseClicked(evt);
            }
        });

        jLabel26.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(255, 0, 0));
        jLabel26.setText("*");

        jLabel30.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        jLabel30.setText("Unidade");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jLabel26)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jListDescricao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextDescricao, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel30)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldUnidadeMedida)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel26)
                    .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldUnidadeMedida, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addComponent(jListDescricao, javax.swing.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
                .addGap(22, 22, 22))
        );

        jButtonSaida.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jButtonSaida.setText("SALVAR");
        jButtonSaida.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonSaidaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonSaidaMouseExited(evt);
            }
        });
        jButtonSaida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaidaActionPerformed(evt);
            }
        });

        jSpinnerQuantidadeRetirada.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jSpinnerQuantidadeRetirada.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSpinnerQuantidadeRetiradaStateChanged(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jLabel20.setText("Quantidade");

        jLabel27.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(255, 0, 0));
        jLabel27.setText("*");

        jButtonAdicionar.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jButtonAdicionar.setText("ADICIONAR");
        jButtonAdicionar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonAdicionarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonAdicionarMouseExited(evt);
            }
        });
        jButtonAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdicionarActionPerformed(evt);
            }
        });

        jLabel28.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(255, 0, 0));
        jLabel28.setText("*");

        javax.swing.GroupLayout jPanelBoxSaidaLayout = new javax.swing.GroupLayout(jPanelBoxSaida);
        jPanelBoxSaida.setLayout(jPanelBoxSaidaLayout);
        jPanelBoxSaidaLayout.setHorizontalGroup(
            jPanelBoxSaidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBoxSaidaLayout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addGroup(jPanelBoxSaidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBoxSaidaLayout.createSequentialGroup()
                        .addComponent(jLabel27)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSpinnerQuantidadeRetirada, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25)
                        .addComponent(jButtonAdicionar, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25)
                        .addComponent(jButtonSaida, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBoxSaidaLayout.createSequentialGroup()
                        .addComponent(jLabel28)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel13)
                        .addGap(18, 18, 18)
                        .addComponent(jFormattedDataSaida, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 847, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 847, Short.MAX_VALUE))
                .addGap(51, 51, 51))
        );
        jPanelBoxSaidaLayout.setVerticalGroup(
            jPanelBoxSaidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBoxSaidaLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanelBoxSaidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jFormattedDataSaida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(jLabel28))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
                .addGap(20, 20, 20)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(jPanelBoxSaidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(jButtonSaida, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel27)
                    .addComponent(jSpinnerQuantidadeRetirada, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonAdicionar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49))
        );

        jLabel14.setBackground(new java.awt.Color(115, 140, 29));
        jLabel14.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("SAÍDA DE MATERIAL DE CONSUMO");
        jLabel14.setOpaque(true);

        jLayeredPaneSaida.setLayer(jPanelBoxSaida, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPaneSaida.setLayer(jLabel14, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPaneSaidaLayout = new javax.swing.GroupLayout(jLayeredPaneSaida);
        jLayeredPaneSaida.setLayout(jLayeredPaneSaidaLayout);
        jLayeredPaneSaidaLayout.setHorizontalGroup(
            jLayeredPaneSaidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPaneSaidaLayout.createSequentialGroup()
                .addContainerGap(269, Short.MAX_VALUE)
                .addGroup(jLayeredPaneSaidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanelBoxSaida, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(268, Short.MAX_VALUE))
        );
        jLayeredPaneSaidaLayout.setVerticalGroup(
            jLayeredPaneSaidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jLayeredPaneSaidaLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanelBoxSaida, javax.swing.GroupLayout.PREFERRED_SIZE, 862, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPrincipal.addTab("Saída", jLayeredPaneSaida);

        jPanelRelatório.setOpaque(false);

        jLabel17.setBackground(new java.awt.Color(115, 140, 29));
        jLabel17.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel17.setText("RELATÓRIOS");
        jLabel17.setOpaque(true);

        jPanelRelatorio.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true), "QUE TIPO DE RELATÓRIO DESEJA EMITIR?", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Century Gothic", 1, 18))); // NOI18N
        jPanelRelatorio.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanelRelatorio.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N

        jButtonRelatorio.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jButtonRelatorio.setText("EMITIR");
        jButtonRelatorio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonRelatorioMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonRelatorioMouseExited(evt);
            }
        });
        jButtonRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRelatorioActionPerformed(evt);
            }
        });

        jCheckBoxEntrada.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jCheckBoxEntrada.setText("ENTRADA");

        jCheckBoxSaida.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jCheckBoxSaida.setText("SAÍDA");
        jCheckBoxSaida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxSaidaActionPerformed(evt);
            }
        });

        try {
            jFormattedDataInicio.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedDataInicio.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jFormattedDataInicio.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jFormattedDataInicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFormattedDataInicioActionPerformed(evt);
            }
        });
        jFormattedDataInicio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jFormattedDataInicioKeyReleased(evt);
            }
        });

        try {
            jFormattedDataFinal.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedDataFinal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jFormattedDataFinal.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jFormattedDataFinal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFormattedDataFinalActionPerformed(evt);
            }
        });
        jFormattedDataFinal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jFormattedDataFinalKeyReleased(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel18.setText("DE");

        jLabel19.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel19.setText("A");

        jLabel23.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel23.setText("POR MATERIAL");

        jButtonEmitirMaterial.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jButtonEmitirMaterial.setText("EMITIR");
        jButtonEmitirMaterial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonEmitirMaterialMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonEmitirMaterialMouseExited(evt);
            }
        });
        jButtonEmitirMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEmitirMaterialActionPerformed(evt);
            }
        });

        jTextRelatorioMaterial.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jTextRelatorioMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextRelatorioMaterialActionPerformed(evt);
            }
        });
        jTextRelatorioMaterial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextRelatorioMaterialKeyReleased(evt);
            }
        });

        jListRelatorioMaterial.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jListRelatorioMaterial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jListRelatorioMaterialMouseClicked(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel22.setText("POR SETOR");

        jButtonEmitirSetor.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        jButtonEmitirSetor.setText("EMITIR");
        jButtonEmitirSetor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonEmitirSetorMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonEmitirSetorMouseExited(evt);
            }
        });
        jButtonEmitirSetor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEmitirSetorActionPerformed(evt);
            }
        });

        jComboBoxSetor.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jComboBoxSetor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jComboBoxSetorMouseClicked(evt);
            }
        });

        jCheckBoxEntradaMaterial.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jCheckBoxEntradaMaterial.setText("ENTRADA");

        jCheckBoxSaidaMaterial.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jCheckBoxSaidaMaterial.setText("SAÍDA");
        jCheckBoxSaidaMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxSaidaMaterialActionPerformed(evt);
            }
        });

        jRadioButtonTodos.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jRadioButtonTodos.setText("TODOS");
        jRadioButtonTodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonTodosActionPerformed(evt);
            }
        });

        try {
            jFormattedDataInicioMaterial.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedDataInicioMaterial.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jFormattedDataInicioMaterial.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N

        jLabel29.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel29.setText("A");

        try {
            jFormattedDataFinalMaterial.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedDataFinalMaterial.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jFormattedDataFinalMaterial.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N

        try {
            jFormattedDataFinalSetor.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedDataFinalSetor.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jFormattedDataFinalSetor.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N

        try {
            jFormattedDataInicioSetor.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedDataInicioSetor.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jFormattedDataInicioSetor.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N

        jLabel31.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabel31.setText("A");

        jCheckBoxDataSetor.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jCheckBoxDataSetor.setText("DE");
        jCheckBoxDataSetor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jCheckBoxDataSetorMouseClicked(evt);
            }
        });

        jCheckBoxDataMaterial.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jCheckBoxDataMaterial.setText("DE");
        jCheckBoxDataMaterial.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jCheckBoxDataMaterialMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanelRelatorioLayout = new javax.swing.GroupLayout(jPanelRelatorio);
        jPanelRelatorio.setLayout(jPanelRelatorioLayout);
        jPanelRelatorioLayout.setHorizontalGroup(
            jPanelRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRelatorioLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(jPanelRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator2)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanelRelatorioLayout.createSequentialGroup()
                        .addGroup(jPanelRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelRelatorioLayout.createSequentialGroup()
                                .addComponent(jCheckBoxEntradaMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jCheckBoxSaidaMaterial)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel23))
                            .addGroup(jPanelRelatorioLayout.createSequentialGroup()
                                .addComponent(jCheckBoxDataMaterial)
                                .addGap(18, 18, 18)
                                .addComponent(jFormattedDataInicioMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28)
                                .addComponent(jLabel29)
                                .addGap(28, 28, 28)
                                .addComponent(jFormattedDataFinalMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextRelatorioMaterial, javax.swing.GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE)
                            .addComponent(jListRelatorioMaterial, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(34, 34, 34)
                        .addComponent(jButtonEmitirMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelRelatorioLayout.createSequentialGroup()
                        .addComponent(jCheckBoxEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBoxSaida)
                        .addGap(65, 65, 65)
                        .addComponent(jLabel18)
                        .addGap(18, 18, 18)
                        .addComponent(jFormattedDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(jLabel19)
                        .addGap(28, 28, 28)
                        .addComponent(jFormattedDataFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(147, 147, 147)
                        .addComponent(jButtonRelatorio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanelRelatorioLayout.createSequentialGroup()
                        .addGroup(jPanelRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelRelatorioLayout.createSequentialGroup()
                                .addComponent(jCheckBoxDataSetor)
                                .addGap(18, 18, 18)
                                .addComponent(jFormattedDataInicioSetor, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28)
                                .addComponent(jLabel31)
                                .addGap(28, 28, 28)
                                .addComponent(jFormattedDataFinalSetor, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanelRelatorioLayout.createSequentialGroup()
                                .addComponent(jLabel22)
                                .addGap(18, 18, 18)
                                .addComponent(jComboBoxSetor, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28)
                                .addComponent(jRadioButtonTodos)
                                .addGap(275, 275, 275)
                                .addComponent(jButtonEmitirSetor, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(50, 50, 50))
        );
        jPanelRelatorioLayout.setVerticalGroup(
            jPanelRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRelatorioLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(jPanelRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jFormattedDataInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jFormattedDataFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(jLabel19)
                    .addComponent(jCheckBoxEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBoxSaida, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonRelatorio, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addGroup(jPanelRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonEmitirMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanelRelatorioLayout.createSequentialGroup()
                        .addGroup(jPanelRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextRelatorioMaterial, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                            .addGroup(jPanelRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jCheckBoxEntradaMaterial)
                                .addComponent(jCheckBoxSaidaMaterial)))
                        .addGroup(jPanelRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jListRelatorioMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanelRelatorioLayout.createSequentialGroup()
                                .addGap(32, 32, 32)
                                .addGroup(jPanelRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jFormattedDataInicioMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel29)
                                    .addComponent(jFormattedDataFinalMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jCheckBoxDataMaterial))))))
                .addGap(34, 34, 34)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addGroup(jPanelRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBoxSetor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22)
                    .addComponent(jButtonEmitirSetor, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jRadioButtonTodos, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(jPanelRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jFormattedDataInicioSetor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31)
                    .addComponent(jFormattedDataFinalSetor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBoxDataSetor))
                .addGap(50, 50, 50))
        );

        javax.swing.GroupLayout jPanelBoxRelatorioLayout = new javax.swing.GroupLayout(jPanelBoxRelatorio);
        jPanelBoxRelatorio.setLayout(jPanelBoxRelatorioLayout);
        jPanelBoxRelatorioLayout.setHorizontalGroup(
            jPanelBoxRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBoxRelatorioLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jPanelRelatorio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50))
        );
        jPanelBoxRelatorioLayout.setVerticalGroup(
            jPanelBoxRelatorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBoxRelatorioLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jPanelRelatorio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50))
        );

        javax.swing.GroupLayout jPanelRelatórioLayout = new javax.swing.GroupLayout(jPanelRelatório);
        jPanelRelatório.setLayout(jPanelRelatórioLayout);
        jPanelRelatórioLayout.setHorizontalGroup(
            jPanelRelatórioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRelatórioLayout.createSequentialGroup()
                .addGap(0, 190, Short.MAX_VALUE)
                .addGroup(jPanelRelatórioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanelBoxRelatorio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(200, Short.MAX_VALUE))
        );
        jPanelRelatórioLayout.setVerticalGroup(
            jPanelRelatórioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelRelatórioLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanelBoxRelatorio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPrincipal.addTab("Relatório", jPanelRelatório);

        jMenuBar1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jMenuBar1.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N

        jMenuConfiguracoes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/icons8_settings_16.png"))); // NOI18N
        jMenuConfiguracoes.setText("Configurações");
        jMenuConfiguracoes.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jMenuConfiguracoes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuConfiguracoesActionPerformed(evt);
            }
        });

        jMenuItemAdministrador.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jMenuItemAdministrador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/icons8_gender_neutral_user_16.png"))); // NOI18N
        jMenuItemAdministrador.setText("Administrador");
        jMenuItemAdministrador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemAdministradorActionPerformed(evt);
            }
        });
        jMenuConfiguracoes.add(jMenuItemAdministrador);

        jMenuBar1.add(jMenuConfiguracoes);

        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/icons8_info_16.png"))); // NOI18N
        jMenu3.setText("Sobre");
        jMenu3.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu3MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, 1491, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(filler2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(filler3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(filler3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(filler2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jTabbedPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, 1177, Short.MAX_VALUE)
        );

        jTabbedPrincipal.getAccessibleContext().setAccessibleName("Sistema de Gerênciamento de Estoque, Gerência de Patrimônio, Materiais e Manutenção");

        setSize(new java.awt.Dimension(1509, 1250));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCadastrarActionPerformed
        Entrada entrada = new Entrada();
        entrada.setCodBarras(jTextCodigoDeBarras.getText());
        entrada.setNotaFiscal(jTextNotaFiscal.getText());

        TipoAquisicao tipoAquisicao = new TipoAquisicao();
        tipoAquisicao.setDescricao(jComboBoxAquisicao.getSelectedItem().toString());
        
        con = ConnectionFactory.getConnection();
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT idtipoAquisicao FROM tipoaquisicao "
                    + "WHERE descricao = '" + tipoAquisicao.getDescricao() + "';");
            rs = stmt.executeQuery();
            rs.first();

            tipoAquisicao.setId(rs.getInt("idtipoAquisicao"));
            entrada.setTipoAquisicaoId(tipoAquisicao);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao pegar ID do tipo de aquisicao\n" + ex);
        } finally {
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }

        String dataString = jFormattedTextData.getText();
        String[] dataSeparada = dataString.split("/");
        LocalDate data = LocalDate.of(Integer.parseInt(dataSeparada[2]), Integer.parseInt(dataSeparada[1]), Integer.parseInt(dataSeparada[0]));

        entrada.setData(data.toString());

        Fornecedor fornecedor = new Fornecedor();
        
        if(!jTextFornecedor.getText().isEmpty()){
            //Fornecedor fornecedor = new Fornecedor();
            fornecedor.setNome(jTextFornecedor.getText());
        
            con = ConnectionFactory.getConnection();
      
            try {
                stmt = (PreparedStatement) con.prepareStatement("SELECT cnpj FROM fornecedor "
                        + "WHERE nome = '" + fornecedor.getNome() + "';");
                rs = stmt.executeQuery();
                rs.first();

                fornecedor.setCnpj(rs.getString("cnpj"));
                entrada.setFornecedorCnpj(fornecedor);

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null,"Erro ao pegar codigo do fornecedor\n" + ex);
            } finally {
                ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
            }
        }else{
            fornecedor.setCnpj(null);
            entrada.setFornecedorCnpj(fornecedor);
        }

        Material material = new Material();
        material.setDescricao(jTextMaterial.getText());
        
        Categoria categoria = new Categoria();
        UnidadeDeMedida unidade = new UnidadeDeMedida();
        
        //if(!jTextFornecedor.getText().equals("")){
          //  material.setDescricao(jTextMaterial.getText());
        con = ConnectionFactory.getConnection();
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM material "
                    + "WHERE descricao = '" + material.getDescricao().replace(ASPASSIMPLES, CONTRABARRAASPASSIMPLES) + "';");
            rs = stmt.executeQuery();
            rs.first();
            
            categoria.setId(rs.getInt("categoria_idCategoria"));
            material.setCategoriaId(categoria);
            unidade.setId(rs.getInt("unidadeMedida_idunidadeMedida"));
            material.setIdUnidadeMedida(unidade);
            material.setIdMaterial(rs.getInt("idMaterial"));
            entrada.setMaterial_idMaterial(material);

        } catch (SQLException ex) {
            System.out.println("Erro ao pegar codigo do Material");
            //JOptionPane.showMessageDialog(null,"Erro ao pegar codigo do Material\n" + ex);
        } finally {
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }//}else{
          //material.setDescricao(null);
          //entrada.setMaterial_idMaterial(material);
      //}
        
        Estoque estoque = new Estoque();
        estoque.setMaterial_idMaterial(material);
        estoque.setEntrada_nNotaFiscal(entrada);
            
        EstoqueDAO estoquedao = new EstoqueDAO();
        
        
        con = ConnectionFactory.getConnection();
        stmt = null;
        rs = null;
        
        try {
            if(jTextMaterial.getText().equals("")){
                JOptionPane.showMessageDialog(getParent(), "Preencha o campo Material."," ALERTA",JOptionPane.ERROR_MESSAGE);
            } else{
                stmt = (PreparedStatement) con.prepareStatement("SELECT material_idMaterial FROM estoque "
                        + "WHERE material_idMaterial = \""+entrada.getMaterial_idMaterial().getIdMaterial()+"\";");
                rs = stmt.executeQuery();

                if(Integer.parseInt(jSpinnerQuantidade.getValue().toString()) > 0 && !jTextNotaFiscal.getText().equals("")){
                    entrada.setQuantidade(Integer.parseInt(jSpinnerQuantidade.getValue().toString()));
                    
                    EntradaDAO entradadao = new EntradaDAO();
                    if(rs.next()){// Verifica se o material existe no estoque
                        //JOptionPane.showMessageDialog(null,"Material existente");

                        if(modoEditar == true){
                            int atual = entrada.getQuantidade();
                            int qtd = 0;
                            if((qtd = entrada.getQuantidade()-aux) != 0){
                                entrada.setQuantidade(qtd);
                                if(estoquedao.somar(estoque) == true){
                                    entrada.setQuantidade(atual);
                                    entradadao.editarEntrada(entrada);
                                    limparCamposEntrada();
                                    preencherTabelaEntrada(entrada);
                                } else{
                                    JOptionPane.showMessageDialog(getParent(), "Erro ao inserir no estoque!"," ALERTA",JOptionPane.ERROR_MESSAGE);
                                }
                            } else{
                                entradadao.editarEntrada(entrada);
                                limparCamposEntrada();
                                preencherTabelaEntrada(entrada);
                            }
                            jSpinnerQuantidade.setEnabled(false);
                            jLabelEditarEntrada.setVisible(false);
                            jTextNotaFiscal.setEditable(true);
                            jTextMaterial.setEditable(true);
                            jButtonCancelarEditar.setVisible(false);
                            aux = 0;
                        } else{
                            if(estoquedao.somar(estoque) == true){// Verifica se o incremento da quantidade ocorreu tudo certo
                            
                                entradadao.efetuarEntrada(entrada);
                                limparCamposEntrada();
                                preencherTabelaEntrada(entrada);
                                jSpinnerQuantidade.setEnabled(false);
                            }else{
                                JOptionPane.showMessageDialog(getParent(), "Erro ao inserir no estoque!"," ALERTA",JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    } else{
                        //JOptionPane.showMessageDialog(null,"Novo material!");
                        if(estoquedao.salvar(estoque) == true && entradadao.efetuarEntrada(entrada) == true){ // Verifica se o novo material foi inserido com sucesso
                            limparCamposEntrada();
                            preencherTabelaEntrada(entrada);
                        } else{
                            JOptionPane.showMessageDialog(getParent(), "Erro ao inserir no estoque!"," ALERTA",JOptionPane.ERROR_MESSAGE);
                        }
                    }
                } else{
                    JOptionPane.showMessageDialog(getParent(), "Preencha os campos obrigatórios"," ALERTA",JOptionPane.WARNING_MESSAGE);
                }
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao verificar estoque\n"+ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }
        //verificarAlertas();
    }//GEN-LAST:event_jButtonCadastrarActionPerformed

    private void jButtonCadastrarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonCadastrarMouseExited
        if(jButtonCadastrar.isEnabled()){
            jButtonCadastrar.setBackground(new Color(240, 240, 240));
            jButtonCadastrar.setIcon(new javax.swing.ImageIcon(""));
            jButtonCadastrar.setText("SALVAR");
        }
    }//GEN-LAST:event_jButtonCadastrarMouseExited

    private void jButtonCadastrarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonCadastrarMouseEntered
        if(jButtonCadastrar.isEnabled()){
            jButtonCadastrar.setBackground(new Color(115,140,29));
            jButtonCadastrar.setText("");
            jButtonCadastrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/icons8_checkmark_32.png")));
        }
    }//GEN-LAST:event_jButtonCadastrarMouseEntered

    private void jTextMaterialKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextMaterialKeyReleased
        if (Enter == 0) {
            listaPesquisaMaterial(jTextMaterial.getText().trim(),MODELOmaterial,jListMaterial);
        } else {
            Enter = 0;
            System.out.println(String.valueOf(MODELOmaterial.getElementAt(jListMaterial.getSelectedIndex())));
            jTextMaterial.setText(String.valueOf(MODELOmaterial.getElementAt(jListMaterial.getSelectedIndex())));
            
            jTextNotaFiscal.requestFocus();
        }
    }//GEN-LAST:event_jTextMaterialKeyReleased

    private void jTextMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextMaterialActionPerformed
        jListMaterial.setVisible(false);
        Enter = 1;
    }//GEN-LAST:event_jTextMaterialActionPerformed

    private void jTextMaterialMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextMaterialMousePressed
        //mostraPesquisaMaterial();
        jListMaterial.setVisible(false);
    }//GEN-LAST:event_jTextMaterialMousePressed

    private void jButtonAdicionarMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarMaterialActionPerformed
        novoMaterial();
    }//GEN-LAST:event_jButtonAdicionarMaterialActionPerformed

    private void jComboBoxAquisicaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxAquisicaoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxAquisicaoActionPerformed

    private void jButtonAdicionarFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarFornecedorActionPerformed
        TelaAdicionarFornecedor telaAdicionarFornecedor = new TelaAdicionarFornecedor();
        telaAdicionarFornecedor.setVisible(true);
    }//GEN-LAST:event_jButtonAdicionarFornecedorActionPerformed

    private void jFormattedTextDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFormattedTextDataActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jFormattedTextDataActionPerformed

    private void jTextFornecedorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFornecedorKeyReleased
        if (Enter == 0) {
            listaPesquisaFornecedor(jTextFornecedor.getText().trim());
        } else {
            con = ConnectionFactory.getConnection();
            try {
                int Linha = jListFornecedor.getSelectedIndex() + 2;//PODE SER APAGADA ESSA LINHA, POREM DEVE SER ALTERADO NO CAMANDO SQL PELO NUMERO 1

                stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM fornecedor "
                        + "WHERE nome like '" + jTextFornecedor.getText() + "%' ORDER BY nome LIMIT " + Linha + ";");
                rs = stmt.executeQuery();
                rs.first();
                
                jTextFornecedor.setForeground(new Color(0,0,0));//Deixa ASPASDUPLAS cor da fonte preta novamente
                //ResultadoPesquisa();
                jTextFornecedor.setText(rs.getString("nome"));

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Erro ao selecionar fornecedor\n" + ex);
            } finally {
                ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
            }
            Enter = 0;
            jSpinnerQuantidade.requestFocus();
        }
    }//GEN-LAST:event_jTextFornecedorKeyReleased

    private void jTextFornecedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFornecedorActionPerformed
        jListFornecedor.setVisible(false);
        Enter = 1;
    }//GEN-LAST:event_jTextFornecedorActionPerformed

    private void jTextFornecedorMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFornecedorMousePressed
        //mostraPesquisaFornecedor();
        jListFornecedor.setVisible(false);
    }//GEN-LAST:event_jTextFornecedorMousePressed

    private void jRadioButtonSetorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonSetorActionPerformed

    }//GEN-LAST:event_jRadioButtonSetorActionPerformed

    private void jListMaterialMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jListMaterialMouseClicked
        jTextMaterial.setText(String.valueOf(MODELOmaterial.getElementAt(jListMaterial.getSelectedIndex())));
        jListMaterial.setVisible(false);
        jTextNotaFiscal.requestFocus();
        if(!jSpinnerQuantidade.isEnabled())
            jSpinnerQuantidade.setEnabled(true);
    }//GEN-LAST:event_jListMaterialMouseClicked

    private void jListFornecedorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jListFornecedorMouseClicked
        jTextFornecedor.setText(String.valueOf(MODELOfornecedor.getElementAt(jListFornecedor.getSelectedIndex())));
        jListFornecedor.setVisible(false);
        jSpinnerQuantidade.requestFocus();
    }//GEN-LAST:event_jListFornecedorMouseClicked

    private void jButtonAdicionarSetorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarSetorActionPerformed

    }//GEN-LAST:event_jButtonAdicionarSetorActionPerformed

    private void jTextCodigoDeBarrasFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextCodigoDeBarrasFocusGained
/*        if(jTextCodigoDeBarras.getText().trim().equals("Insira o código de barras")){
            jTextCodigoDeBarras.setText("");
            jTextCodigoDeBarras.setForeground(new Color(0,0,0));
        }*/
    }//GEN-LAST:event_jTextCodigoDeBarrasFocusGained

    private void jTextCodigoDeBarrasFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextCodigoDeBarrasFocusLost
        /*if(jTextCodigoDeBarras.getText().trim().equals("")||
                jTextCodigoDeBarras.getText().trim().toLowerCase().equals("Insira o código de barras")){
            jTextCodigoDeBarras.setText("Insira o código de barras");
            jTextCodigoDeBarras.setForeground(new Color(153,153,153));
        }*/
    }//GEN-LAST:event_jTextCodigoDeBarrasFocusLost

    private void jTextNotaFiscalFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextNotaFiscalFocusGained
       /* if(jTextNotaFiscal.getText().trim().equals("Insira o número da nota fiscal")){
            jTextNotaFiscal.setText("");
            jTextNotaFiscal.setForeground(new Color(0,0,0));
        }*/
    }//GEN-LAST:event_jTextNotaFiscalFocusGained

    private void jTextNotaFiscalFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextNotaFiscalFocusLost
        /*if(jTextNotaFiscal.getText().trim().equals("")||
            jTextNotaFiscal.getText().trim().toLowerCase().equals("Insira o número da nota fiscal")){
            jTextNotaFiscal.setText("Insira o número da nota fiscal");
            jTextNotaFiscal.setForeground(new Color(153,153,153));
        }*/
    }//GEN-LAST:event_jTextNotaFiscalFocusLost

    private void jTextFornecedorFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFornecedorFocusGained
        /*if(jTextFornecedor.getText().trim().equals("Digite o nome do fornecedor")){
            jTextFornecedor.setText("");
            jTextFornecedor.setForeground(new Color(0,0,0));
        }*/
    }//GEN-LAST:event_jTextFornecedorFocusGained

    private void jTextFornecedorFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFornecedorFocusLost
       /* if(jTextFornecedor.getText().trim().equals("")||
            jTextFornecedor.getText().trim().toLowerCase().equals("Digite o nome do fornecedor")){
            jTextFornecedor.setText("Digite o nome do fornecedor");
            jTextFornecedor.setForeground(new Color(153,153,153));
        }*/
    }//GEN-LAST:event_jTextFornecedorFocusLost

    private void jTextMaterialFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextMaterialFocusGained
        /*if(jTextMaterial.getText().equals("Digite o nome do material")){
            jTextMaterial.setText("");
            jTextMaterial.setForeground(new Color(0,0,0));
        }*/
    }//GEN-LAST:event_jTextMaterialFocusGained

    private void jTextMaterialFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextMaterialFocusLost
       /* if(jTextMaterial.getText().equals("")||
            jTextMaterial.getText().toLowerCase().equals("Digite o nome do material")){
            jTextMaterial.setText("Digite o nome do material");
            jTextMaterial.setForeground(new Color(153,153,153));
        }*/
    }//GEN-LAST:event_jTextMaterialFocusLost

    private void jButtonRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRelatorioActionPerformed
        if(!jFormattedDataInicio.getText().equals("  /  /    ") && !jFormattedDataFinal.getText().equals("  /  /    ")){
            String dataString1 = jFormattedDataInicio.getText();
            String[] dataSeparada1 = dataString1.split("/");
            LocalDate dataInicio = LocalDate.of(Integer.parseInt(dataSeparada1[2]), Integer.parseInt(dataSeparada1[1]), Integer.parseInt(dataSeparada1[0]));

            String dataString2 = jFormattedDataFinal.getText();
            String[] dataSeparada2 = dataString2.split("/");
            LocalDate dataFinal = LocalDate.of(Integer.parseInt(dataSeparada2[2]), Integer.parseInt(dataSeparada2[1]), Integer.parseInt(dataSeparada2[0]));

            String sqlEntrada = "SELECT entrada.dataEntrada,material.descricao,entrada.nNotaFiscal,"
                        + "fornecedor.nome,entrada.quantidade,fornecedor.cnpj,tipoaquisicao.descricao FROM entrada INNER JOIN material ON"
                        + " entrada.material_idMaterial = material.idMaterial LEFT JOIN fornecedor ON entrada.fornecedor_cnpj "
                        + "= fornecedor.cnpj INNER JOIN tipoaquisicao ON entrada.tipoaquisicao_idtipoAquisicao = "
                        + "tipoaquisicao.idtipoAquisicao WHERE "
                        + "entrada.dataEntrada BETWEEN '"+dataInicio+"' AND '"+dataFinal+"\"' ORDER BY entrada.dataEntrada;";

            String qtdEntrada = "SELECT COUNT(dataEntrada) as TOTAL FROM entrada WHERE dataEntrada BETWEEN '"+dataInicio+"' AND '"+dataFinal+"';";

            String sqlSaida = "SELECT saida.dataSaida, material.descricao, "
                        + "saida.quantidade, solicitante.nome, solicitante.setor FROM saida INNER JOIN material "
                        + "ON saida.material_idMaterial = material.idMaterial LEFT JOIN solicitante "
                        + "ON saida.solicitante_id = solicitante.id WHERE saida.dataSaida "
                        + "BETWEEN '"+dataInicio+"' AND '"+dataFinal+"' ORDER BY saida.dataSaida;";

            String qtdSaida = "SELECT COUNT(dataSaida) as TOTAL FROM saida WHERE dataSaida BETWEEN '"+dataInicio+"' AND '"+dataFinal+"';";

            Document document = new Document();

            try {
                try {
                    PdfWriter.getInstance(document, new FileOutputStream("Relatorio.pdf"));
                    document.open();
                    document.setPageSize(PageSize.A4);
                    /* try {
                        Image imagem = Image.getInstance("nomedaimagem.formato");
                        imagem.scaleToFit(395,85);
                        document.add(imagem);

                    } catch (BadElementException | IOException ex) {
                        JOptionPane.showMessageDialog(null,"Erro ao inserir imagem no pdf\n"+ex);
                    }*/
                    Paragraph paragrafo;
                    paragrafo = new Paragraph("");
                    document.add(paragrafo);

                    if(jCheckBoxEntrada.isSelected() && jCheckBoxSaida.isSelected()){
                        checkBoxEntradaMaterial(document, paragrafo, sqlEntrada, qtdEntrada);
                        checkBoxSaidaMaterial(document, paragrafo, sqlSaida, qtdSaida);
                    } else if(!jCheckBoxEntrada.isSelected() && jCheckBoxSaida.isSelected()){
                        checkBoxSaidaMaterial(document, paragrafo, sqlSaida, qtdSaida);
                    } else if(jCheckBoxEntrada.isSelected() && !jCheckBoxSaida.isSelected()){
                        checkBoxEntradaMaterial(document, paragrafo, sqlEntrada, qtdEntrada);
                    } else{
                        JOptionPane.showMessageDialog(null,"Selecione pelo menos uma opção (entrada/saída).");
                    }

                } catch (FileNotFoundException ex) {
                    JOptionPane.showMessageDialog(null,"Erro ao gerar pdf\n"+ex);
                } finally{
                    document.close();
                }

                try {
                    Desktop.getDesktop().open(new File("Relatorio.pdf"));
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(null,"Erro ao mostrar pdf\n"+ex);
                }

            } catch (DocumentException ex) {
                Logger.getLogger(TelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else{
            JOptionPane.showMessageDialog(null,"Verifique as datas.");
        }
    }//GEN-LAST:event_jButtonRelatorioActionPerformed

    private void jButtonFornecedorMaisMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonFornecedorMaisMouseEntered
        jLabelFornecedor.setForeground(new Color(115,140,29));
        jButtonFornecedorMais.setBackground(new Color(115,140,29));
    }//GEN-LAST:event_jButtonFornecedorMaisMouseEntered

    private void jButtonFornecedorMaisMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonFornecedorMaisMouseExited
        jLabelFornecedor.setForeground(Color.BLACK);
        jButtonFornecedorMais.setBackground(new Color(240,240,240));
    }//GEN-LAST:event_jButtonFornecedorMaisMouseExited

    private void jButtonFornecedorVisualizarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonFornecedorVisualizarMouseEntered
        jLabelFornecedor.setForeground(new Color(115,140,29));
        jButtonFornecedorVisualizar.setBackground(new Color(115,140,29));
    }//GEN-LAST:event_jButtonFornecedorVisualizarMouseEntered

    private void jButtonFornecedorVisualizarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonFornecedorVisualizarMouseExited
        jLabelFornecedor.setForeground(Color.BLACK);
        jButtonFornecedorVisualizar.setBackground(new Color(240,240,240));
    }//GEN-LAST:event_jButtonFornecedorVisualizarMouseExited

    private void jButtonSolicitantesMaisMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSolicitantesMaisMouseEntered
        jLabelSolicitantes.setForeground(new Color(115,140,29));
        jButtonSolicitantesMais.setBackground(new Color(115,140,29));
    }//GEN-LAST:event_jButtonSolicitantesMaisMouseEntered

    private void jButtonSolicitantesMaisMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSolicitantesMaisMouseExited
        jLabelSolicitantes.setForeground(Color.BLACK);
        jButtonSolicitantesMais.setBackground(new Color(240,240,240));
    }//GEN-LAST:event_jButtonSolicitantesMaisMouseExited

    private void jButtonSolicitantesVisualizarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSolicitantesVisualizarMouseEntered
        jLabelSolicitantes.setForeground(new Color(115,140,29));
        jButtonSolicitantesVisualizar.setBackground(new Color(115,140,29));
    }//GEN-LAST:event_jButtonSolicitantesVisualizarMouseEntered

    private void jButtonSolicitantesVisualizarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSolicitantesVisualizarMouseExited
        jLabelSolicitantes.setForeground(Color.BLACK);
        jButtonSolicitantesVisualizar.setBackground(new Color(240,240,240));
    }//GEN-LAST:event_jButtonSolicitantesVisualizarMouseExited

    private void jButtonEstoqueVisualizarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonEstoqueVisualizarMouseEntered
        jLabelEstoque.setForeground(new Color(115,140,29));
        jButtonEstoqueVisualizar.setBackground(new Color(115,140,29));
    }//GEN-LAST:event_jButtonEstoqueVisualizarMouseEntered

    private void jButtonEstoqueVisualizarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonEstoqueVisualizarMouseExited
        jLabelEstoque.setForeground(Color.BLACK);
        jButtonEstoqueVisualizar.setBackground(new Color(240,240,240));
    }//GEN-LAST:event_jButtonEstoqueVisualizarMouseExited

    private void jButtonEntradasVisualizarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonEntradasVisualizarMouseEntered
        jLabelEntradas.setForeground(new Color(115,140,29));
        jButtonEntradasVisualizar.setBackground(new Color(115,140,29));
    }//GEN-LAST:event_jButtonEntradasVisualizarMouseEntered

    private void jButtonEntradasVisualizarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonEntradasVisualizarMouseExited
        jLabelEntradas.setForeground(Color.BLACK);
        jButtonEntradasVisualizar.setBackground(new Color(240,240,240));
    }//GEN-LAST:event_jButtonEntradasVisualizarMouseExited

    private void jButtonSaidasVisualizarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSaidasVisualizarMouseEntered
        jLabelSaidas.setForeground(new Color(115,140,29));
        jButtonSaidasVisualizar.setBackground(new Color(115,140,29));
    }//GEN-LAST:event_jButtonSaidasVisualizarMouseEntered

    private void jButtonSaidasVisualizarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSaidasVisualizarMouseExited
        jLabelSaidas.setForeground(Color.BLACK);
        jButtonSaidasVisualizar.setBackground(new Color(240,240,240));
    }//GEN-LAST:event_jButtonSaidasVisualizarMouseExited

    private void jButtonFornecedorMaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonFornecedorMaisActionPerformed
        novoFornecedor();
    }//GEN-LAST:event_jButtonFornecedorMaisActionPerformed

    private void jButtonSolicitantesMaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSolicitantesMaisActionPerformed
        novoSolicitante();
    }//GEN-LAST:event_jButtonSolicitantesMaisActionPerformed

    private void jButtonEstoqueVisualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEstoqueVisualizarActionPerformed
        visualizarEstoque();
    }//GEN-LAST:event_jButtonEstoqueVisualizarActionPerformed

    private void jButtonFornecedorVisualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonFornecedorVisualizarActionPerformed
        visualizarFornecedores();
    }//GEN-LAST:event_jButtonFornecedorVisualizarActionPerformed

    private void jButtonSolicitantesVisualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSolicitantesVisualizarActionPerformed
        visualizarSolicitantes();
    }//GEN-LAST:event_jButtonSolicitantesVisualizarActionPerformed

    private void jButtonEntradasVisualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEntradasVisualizarActionPerformed
        visualizarEntradas();
    }//GEN-LAST:event_jButtonEntradasVisualizarActionPerformed

    private void jButtonSaidasVisualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaidasVisualizarActionPerformed
        visualizarSaidas();
    }//GEN-LAST:event_jButtonSaidasVisualizarActionPerformed

    private void jButtonEmitirSetorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEmitirSetorActionPerformed
        String sqlSaidaTodos = "SELECT saida.dataSaida, material.descricao, saida.quantidade, "
                            + "solicitante.nome, solicitante.setor FROM saida INNER JOIN material ON saida.material_idMaterial = "
                            + "material.idMaterial LEFT JOIN solicitante ON saida.solicitante_id = solicitante.id";
        
        String qtdTodos = "SELECT COUNT(solicitante_id) as TOTAL FROM saida";
        
        String sqlSaidaSetor = "SELECT saida.dataSaida, material.descricao, saida.quantidade, "
                            + "solicitante.nome, solicitante.setor FROM saida INNER JOIN material ON saida.material_idMaterial = "
                            + "material.idMaterial LEFT JOIN solicitante ON saida.solicitante_id = solicitante.id "
                            + "WHERE solicitante.setor = \""+jComboBoxSetor.getSelectedItem()+"\"";
        
        String qtdSetor = "SELECT COUNT(setor) as TOTAL FROM saida "
                        + "LEFT JOIN solicitante ON saida.solicitante_id = solicitante.id "
                        + "WHERE solicitante.setor = \""+jComboBoxSetor.getSelectedItem()+"\"";
        
        Document document = new Document();
        
        try {
            PdfWriter.getInstance(document, new FileOutputStream("RelatorioSetores.pdf"));
            
            document.open();
            document.setPageSize(PageSize.A4);

            Paragraph paragrafo;
            paragrafo = new Paragraph("");
            document.add(paragrafo);
            
            if(jCheckBoxDataSetor.isSelected()){
                String dataString1 = jFormattedDataInicioSetor.getText();
                String[] dataSeparada1 = dataString1.split("/");
                LocalDate dataInicio = LocalDate.of(Integer.parseInt(dataSeparada1[2]), Integer.parseInt(dataSeparada1[1]), Integer.parseInt(dataSeparada1[0]));

                String dataString2 = jFormattedDataFinalSetor.getText();
                String[] dataSeparada2 = dataString2.split("/");
                LocalDate dataFinal = LocalDate.of(Integer.parseInt(dataSeparada2[2]), Integer.parseInt(dataSeparada2[1]), Integer.parseInt(dataSeparada2[0]));
         
                String intervaloDataSaida = "saida.dataSaida BETWEEN '"+dataInicio+"' AND '"+dataFinal+"' ORDER BY solicitante.setor ASC;";
                
                if(jRadioButtonTodos.isSelected()){
                    checkBoxSaidaMaterial(document, paragrafo, sqlSaidaTodos.concat(" WHERE ").concat(intervaloDataSaida), qtdTodos.concat(" WHERE ").concat(intervaloDataSaida));
                } else{
                    checkBoxSaidaMaterial(document, paragrafo, sqlSaidaSetor.concat(" AND ").concat(intervaloDataSaida), qtdSetor.concat(" AND ").concat(intervaloDataSaida));
                }
            } else{
                if(jRadioButtonTodos.isSelected()){
                    checkBoxSaidaMaterial(document, paragrafo, sqlSaidaTodos.concat(" ORDER BY solicitante.setor ASC"), qtdTodos);
                } else{
                    checkBoxSaidaMaterial(document, paragrafo, sqlSaidaSetor, qtdSetor);
                }
            }

        } catch (FileNotFoundException | DocumentException ex) {
            Logger.getLogger(TelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            document.close();
        }
        
        try {
                Desktop.getDesktop().open(new File("RelatorioSetores.pdf"));
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao mostrar pdf\n"+ex);
        }
    }//GEN-LAST:event_jButtonEmitirSetorActionPerformed

    private void jTextRelatorioMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextRelatorioMaterialActionPerformed
        jListRelatorioMaterial.setVisible(false);
        Enter = 1;
    }//GEN-LAST:event_jTextRelatorioMaterialActionPerformed

    private void jTextRelatorioMaterialKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextRelatorioMaterialKeyReleased
        if (Enter == 0) {
            listaPesquisaRelatorioMaterial();
        } else {
            con = ConnectionFactory.getConnection();
            try {
                int Linha = jListRelatorioMaterial.getSelectedIndex() + 2;

                stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM material "
                        + "WHERE descricao like '" + jTextRelatorioMaterial.getText() + "%' ORDER BY descricao LIMIT " + Linha + ";");
                rs = stmt.executeQuery();
                rs.first();

                jTextRelatorioMaterial.setText(rs.getString("descricao"));

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Erro ao selecionar a descrição do material\n" + ex);
            } finally {
                ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
            }
            Enter = 0;
        }
    }//GEN-LAST:event_jTextRelatorioMaterialKeyReleased

    private void jListRelatorioMaterialMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jListRelatorioMaterialMouseClicked
        jTextRelatorioMaterial.setText(jListRelatorioMaterial.getSelectedValue());
        jListRelatorioMaterial.setVisible(false);
    }//GEN-LAST:event_jListRelatorioMaterialMouseClicked

    private void jButtonRelatorioMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonRelatorioMouseEntered
        jButtonRelatorio.setBackground(new Color(115,140,29));
        jButtonRelatorio.setText("");
        jButtonRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/pdf-2_26px.png")));
    }//GEN-LAST:event_jButtonRelatorioMouseEntered

    private void jButtonRelatorioMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonRelatorioMouseExited
        jButtonRelatorio.setBackground(new Color(240,240,240));
        jButtonRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        jButtonRelatorio.setText("EMITIR");
    }//GEN-LAST:event_jButtonRelatorioMouseExited

    private void jButtonEmitirSetorMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonEmitirSetorMouseEntered
        jButtonEmitirSetor.setBackground(new Color(115,140,29));
        jButtonEmitirSetor.setText("");
        jButtonEmitirSetor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/pdf-2_26px.png")));
    }//GEN-LAST:event_jButtonEmitirSetorMouseEntered

    private void jButtonEmitirSetorMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonEmitirSetorMouseExited
        jButtonEmitirSetor.setBackground(new Color(240,240,240));
        jButtonEmitirSetor.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        jButtonEmitirSetor.setText("EMITIR");
    }//GEN-LAST:event_jButtonEmitirSetorMouseExited

    private void jButtonEmitirMaterialMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonEmitirMaterialMouseEntered
        jButtonEmitirMaterial.setBackground(new Color(115,140,29));
        jButtonEmitirMaterial.setText("");
        jButtonEmitirMaterial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/pdf-2_26px.png")));
    }//GEN-LAST:event_jButtonEmitirMaterialMouseEntered

    private void jButtonEmitirMaterialMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonEmitirMaterialMouseExited
        jButtonEmitirMaterial.setBackground(new Color(240,240,240));
        jButtonEmitirMaterial.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        jButtonEmitirMaterial.setText("EMITIR");
    }//GEN-LAST:event_jButtonEmitirMaterialMouseExited

    private void jButtonEmitirMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEmitirMaterialActionPerformed
         
        
        String entradaMaterial = "SELECT entrada.dataEntrada, entrada.nNotaFiscal, material.descricao, entrada.quantidade, "
                    + "fornecedor.nome, fornecedor.cnpj, tipoaquisicao.descricao FROM entrada INNER JOIN material ON entrada.material_idMaterial = "
                    + "material.idMaterial LEFT JOIN fornecedor ON entrada.fornecedor_cnpj = fornecedor.cnpj INNER JOIN tipoaquisicao ON "
                    + "entrada.tipoAquisicao_idtipoAquisicao = tipoaquisicao.idtipoAquisicao "
                    + "WHERE material.descricao = \""+jTextRelatorioMaterial.getText()+"\"";
        
        String qtdEntrada = "SELECT COUNT(material.descricao) as TOTAL FROM entrada "
                    + "INNER JOIN material ON entrada.material_idMaterial = material.idMaterial "
                    + "WHERE material.descricao = \""+jTextRelatorioMaterial.getText()+"\"";
        
        String saidaMaterial = "SELECT saida.dataSaida, material.descricao, saida.quantidade, "
                    + "solicitante.nome, solicitante.setor FROM saida INNER JOIN material ON saida.material_idMaterial = "
                    + "material.idMaterial LEFT JOIN solicitante ON saida.solicitante_id = solicitante.id "
                    + "WHERE material.descricao = \""+jTextRelatorioMaterial.getText()+"\"";
        
        String qtdSaida = "SELECT COUNT(material.descricao) as TOTAL FROM saida "
                    + "INNER JOIN material ON saida.material_idMaterial = material.idMaterial "
                    + "WHERE material.descricao = \""+jTextRelatorioMaterial.getText()+"\"";
        
        
        if(!jCheckBoxEntradaMaterial.isSelected() && !jCheckBoxSaidaMaterial.isSelected()){
            JOptionPane.showMessageDialog(null,"Atenção\nSelecione entrada, saída ou ambos!");
        } else if(jTextRelatorioMaterial.getText().equals("")){
            JOptionPane.showMessageDialog(null,"Campo material vazio, verifique!");
        } else{
            
            Document document = new Document();

            try {
                PdfWriter.getInstance(document, new FileOutputStream("RelatorioMateriais.pdf"));

                document.open();
                document.setPageSize(PageSize.A4);

                Paragraph paragrafo;
                paragrafo = new Paragraph("");
                document.add(paragrafo);
                    
                if(jCheckBoxDataMaterial.isSelected() && !jFormattedDataInicioMaterial.getText().equals("  /  /    ") && !jFormattedDataFinalMaterial.getText().equals("  /  /    ")){
                    String dataString1 = jFormattedDataInicioMaterial.getText();
                    String[] dataSeparada1 = dataString1.split("/");
                    LocalDate dataInicio = LocalDate.of(Integer.parseInt(dataSeparada1[2]), Integer.parseInt(dataSeparada1[1]), Integer.parseInt(dataSeparada1[0]));

                    String dataString2 = jFormattedDataFinalMaterial.getText();
                    String[] dataSeparada2 = dataString2.split("/");
                    LocalDate dataFinal = LocalDate.of(Integer.parseInt(dataSeparada2[2]), Integer.parseInt(dataSeparada2[1]), Integer.parseInt(dataSeparada2[0]));

                    String intervaloDataEntrada = " AND entrada.dataEntrada BETWEEN '"+dataInicio+"' AND '"+dataFinal+"' ORDER BY entrada.dataEntrada;";            
                    String intervaloDataSaida = " AND saida.dataSaida BETWEEN '"+dataInicio+"' AND '"+dataFinal+"' ORDER BY saida.dataSaida;";
                    
                    if(jCheckBoxEntradaMaterial.isSelected() == true && jCheckBoxSaidaMaterial.isSelected() == true){
                        checkBoxEntradaMaterial(document, paragrafo, entradaMaterial.concat(intervaloDataEntrada), qtdEntrada.concat(intervaloDataEntrada));
                        checkBoxSaidaMaterial(document, paragrafo, saidaMaterial.concat(intervaloDataSaida), qtdSaida.concat(intervaloDataSaida));
                    }
                    if(jCheckBoxEntradaMaterial.isSelected() == true && jCheckBoxSaidaMaterial.isSelected() == false){
                        checkBoxEntradaMaterial(document, paragrafo, entradaMaterial.concat(intervaloDataEntrada), qtdEntrada.concat(intervaloDataEntrada));
                    }
                    if(jCheckBoxSaidaMaterial.isSelected() == true && jCheckBoxEntradaMaterial.isSelected() == false){
                        checkBoxSaidaMaterial(document, paragrafo, saidaMaterial.concat(intervaloDataSaida), qtdSaida.concat(intervaloDataSaida));
                    }
                } else if(!jCheckBoxDataMaterial.isSelected()){
                    if(jCheckBoxEntradaMaterial.isSelected() == true && jCheckBoxSaidaMaterial.isSelected() == true){
                        checkBoxEntradaMaterial(document, paragrafo, entradaMaterial, qtdEntrada);
                        checkBoxSaidaMaterial(document, paragrafo, saidaMaterial, qtdSaida);
                    }
                    if(jCheckBoxEntradaMaterial.isSelected() == true && jCheckBoxSaidaMaterial.isSelected() == false){
                        checkBoxEntradaMaterial(document, paragrafo, entradaMaterial, qtdEntrada);
                    }
                    if(jCheckBoxSaidaMaterial.isSelected() == true && jCheckBoxEntradaMaterial.isSelected() == false){
                        checkBoxSaidaMaterial(document, paragrafo, saidaMaterial, qtdSaida);
                    }
                } else{
                    JOptionPane.showMessageDialog(null,"Data inválida, verifique.");
                }
            } catch (FileNotFoundException | DocumentException ex) {
                Logger.getLogger(TelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            } finally{
                document.close();
            }

            try {
                    Desktop.getDesktop().open(new File("RelatorioMateriais.pdf"));
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null,"Erro ao mostrar pdf\n"+ex);
            }
        }
    }//GEN-LAST:event_jButtonEmitirMaterialActionPerformed

    private void jButtonSaidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaidaActionPerformed
        if(lista.isEmpty())
            JOptionPane.showMessageDialog(getParent(), "Nenhuma saída adicionada."," ALERTA",JOptionPane.WARNING_MESSAGE);
        else{
            SaidaDAO sDAO = new SaidaDAO();
            EstoqueDAO eDAO = new EstoqueDAO();
            
            Boolean sucess = false;
            
            for(Saida saida : lista){
                Estoque estoque = new Estoque();
                estoque.setMaterial_idMaterial(saida.getMaterial_idMaterial());
                estoque.setQuantidade(saida.getQuantidade());
                estoque.setSaida_idSaida(saida);
                if(eDAO.remover(estoque)){
                    if(sDAO.efetuarSaida(saida))
                        sucess = true;
                    else{
                        sucess = false;
                        break;
                    }
                }
            }
            
            if(sucess){
                JOptionPane.showMessageDialog(null,"Saída efetuada com sucesso!");
            }else{
                JOptionPane.showMessageDialog(getParent(),"Erro ao efetuar saída!","Aviso",JOptionPane.WARNING_MESSAGE);
            }
            
            limparCamposSaida();
            bloquearCamposSaida();
            lista.clear();
            linhas.clear();
            //verificarAlertas();
        }
    }//GEN-LAST:event_jButtonSaidaActionPerformed

    private void jButtonSaidaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSaidaMouseExited
        if(jButtonSaida.isEnabled()){
            jButtonSaida.setBackground(new Color(240,240,240));
            jButtonSaida.setForeground(Color.BLACK);
            jButtonSaida.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
            jButtonSaida.setText("SALVAR");
        }
    }//GEN-LAST:event_jButtonSaidaMouseExited

    private void jButtonSaidaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSaidaMouseEntered
        if(jButtonSaida.isEnabled()){
            jButtonSaida.setText("");
            jButtonSaida.setBackground(new Color(115,140,29));
            //jButtonSaida.setForeground(Color.WHITE);
            jButtonSaida.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/icons8_checkmark_32.png")));
        }
    }//GEN-LAST:event_jButtonSaidaMouseEntered

    private void jListDescricaoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jListDescricaoMouseClicked
        jTextDescricao.setText(String.valueOf(MODELOdescricao.getElementAt(jListDescricao.getSelectedIndex())));
        Material m;
        m = (Material) MODELOdescricao.getElementAt(jListDescricao.getSelectedIndex());
        jTextFieldUnidadeMedida.setText(String.valueOf(m.getIdUnidadeMedida().getTipo()));
        jListDescricao.setVisible(false);
        
        jSpinnerQuantidadeRetirada.setEnabled(true);
    }//GEN-LAST:event_jListDescricaoMouseClicked

    private void jTextDescricaoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextDescricaoKeyReleased
        if (Enter == 0) {
            listaPesquisaMaterial(jTextDescricao.getText().trim(),MODELOdescricao, jListDescricao);
        } else {
            jTextDescricao.setText(String.valueOf(MODELOdescricao.getElementAt(jListDescricao.getSelectedIndex())));
            Enter = 0;
            jSpinnerQuantidadeRetirada.requestFocus();
            
            //if(!jTextDescricao.getText().trim().isEmpty())
            //    bloquearCamposSaida(jSpinnerQuantidadeRetirada, jButtonAdicionar);
        }
    }//GEN-LAST:event_jTextDescricaoKeyReleased

    private void jTextDescricaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextDescricaoActionPerformed
//        jListDescricao.setVisible(false);
        Enter = 1;
    }//GEN-LAST:event_jTextDescricaoActionPerformed

    private void jButtonMaterialMaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonMaterialMaisActionPerformed
        novoMaterial();
    }//GEN-LAST:event_jButtonMaterialMaisActionPerformed

    private void jButtonMaterialMaisMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonMaterialMaisMouseEntered
        jButtonMaterialMais.setBackground(new Color(115,140,29));
        jLabelMaterialMais.setForeground(new Color(115,140,29));
    }//GEN-LAST:event_jButtonMaterialMaisMouseEntered

    private void jButtonMaterialMaisMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonMaterialMaisMouseExited
        jButtonMaterialMais.setBackground(new Color(240,240,240));
        jLabelMaterialMais.setForeground(Color.BLACK);
    }//GEN-LAST:event_jButtonMaterialMaisMouseExited

    private void jComboBoxSetorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jComboBoxSetorMouseClicked
        if(jRadioButtonTodos.isSelected() == false){
            jComboBoxSetor.removeAllItems();
            listarComboBoxSetor();
        }
    }//GEN-LAST:event_jComboBoxSetorMouseClicked

    private void jRadioButtonTodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonTodosActionPerformed
        jComboBoxSetor.removeAllItems();
        if(!jRadioButtonTodos.isSelected()){
            jComboBoxSetor.setEnabled(true);
            listarComboBoxSetor();
        } else{
            jComboBoxSetor.setEnabled(false);
        }
    }//GEN-LAST:event_jRadioButtonTodosActionPerformed

    private void jCheckBoxSaidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxSaidaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBoxSaidaActionPerformed

    private void jCheckBoxSaidaMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxSaidaMaterialActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBoxSaidaMaterialActionPerformed

    private void jTextNotaFiscalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextNotaFiscalActionPerformed
        Enter = 1;
    }//GEN-LAST:event_jTextNotaFiscalActionPerformed

    private void jCheckBoxDataMaterialMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCheckBoxDataMaterialMouseClicked
        if(jCheckBoxDataMaterial.isSelected()){
            jFormattedDataInicioMaterial.setEnabled(true);
            jFormattedDataFinalMaterial.setEnabled(true);
        } else{
            jFormattedDataInicioMaterial.setEnabled(false);
            jFormattedDataFinalMaterial.setEnabled(false);
        }
        
    }//GEN-LAST:event_jCheckBoxDataMaterialMouseClicked

    private void jCheckBoxDataSetorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCheckBoxDataSetorMouseClicked
        if(jCheckBoxDataSetor.isSelected()){
            jFormattedDataInicioSetor.setEnabled(true);
            jFormattedDataFinalSetor.setEnabled(true);
        } else{
            jFormattedDataInicioSetor.setEnabled(false);
            jFormattedDataFinalSetor.setEnabled(false);
        }
    }//GEN-LAST:event_jCheckBoxDataSetorMouseClicked

    private void jButtonEntradaMaisMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonEntradaMaisMouseEntered
        jLabelEntradas.setForeground(new Color(115,140,29));
        jButtonEntradaMais.setBackground(new Color(115,140,29));
    }//GEN-LAST:event_jButtonEntradaMaisMouseEntered

    private void jButtonEntradaMaisMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonEntradaMaisMouseExited
        jLabelEntradas.setForeground(Color.BLACK);
        jButtonEntradaMais.setBackground(new Color(240,240,240));
    }//GEN-LAST:event_jButtonEntradaMaisMouseExited

    private void jButtonEntradaMaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEntradaMaisActionPerformed
        irParaEntrada();
    }//GEN-LAST:event_jButtonEntradaMaisActionPerformed

    private void jButtonSaidaMaisMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSaidaMaisMouseEntered
        jLabelSaidas.setForeground(new Color(115,140,29));
        jButtonSaidaMais.setBackground(new Color(115,140,29));
    }//GEN-LAST:event_jButtonSaidaMaisMouseEntered

    private void jButtonSaidaMaisMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSaidaMaisMouseExited
        jLabelSaidas.setForeground(Color.BLACK);
        jButtonSaidaMais.setBackground(new Color(240,240,240));
    }//GEN-LAST:event_jButtonSaidaMaisMouseExited

    private void jButtonSaidaMaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaidaMaisActionPerformed
        jTabbedPrincipal.setSelectedComponent(jLayeredPaneSaida);
    }//GEN-LAST:event_jButtonSaidaMaisActionPerformed

    private void jButtonAlertaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAlertaMouseEntered
        jLabelAlertas.setForeground(new Color(115,140,29));
        jButtonAlerta.setBackground(new Color(115,140,29));
    }//GEN-LAST:event_jButtonAlertaMouseEntered

    private void jButtonAlertaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAlertaMouseExited
//        AlertaDAO alerta = new AlertaDAO();
//        int qtd = 0;
//        if((qtd = alerta.quantidadeOcorrencias()) > 0){
//            modoAlerta(qtd);
//        } else{
            jLabelAlertas.setForeground(Color.BLACK);
            jButtonAlerta.setBackground(new Color(240,240,240));
//        }
    }//GEN-LAST:event_jButtonAlertaMouseExited

    private void jButtonAlertaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAlertaActionPerformed
        alertaVisualizado = true;
        TelaAlertaQtdMinima alerta = new TelaAlertaQtdMinima();
        alerta.setVisible(true);
    }//GEN-LAST:event_jButtonAlertaActionPerformed

    private void jButtonAdicionarFornecedorMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAdicionarFornecedorMouseEntered
        jButtonAdicionarFornecedor.setBackground(new Color(115,140,29));
        jButtonAdicionarFornecedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/plus_math_26px.png")));
    }//GEN-LAST:event_jButtonAdicionarFornecedorMouseEntered

    private void jButtonAdicionarFornecedorMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAdicionarFornecedorMouseExited
        jButtonAdicionarFornecedor.setBackground(new Color(240,240,240));
        jButtonAdicionarFornecedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/plus_math_black_26px.png")));
        
    }//GEN-LAST:event_jButtonAdicionarFornecedorMouseExited

    private void jButtonAdicionarMaterialMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAdicionarMaterialMouseEntered
        jButtonAdicionarMaterial.setBackground(new Color(115,140,29));
        jButtonAdicionarMaterial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/plus_math_26px.png")));
    }//GEN-LAST:event_jButtonAdicionarMaterialMouseEntered

    private void jButtonAdicionarMaterialMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAdicionarMaterialMouseExited
        jButtonAdicionarMaterial.setBackground(new Color(240,240,240));
        jButtonAdicionarMaterial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/plus_math_black_26px.png")));
    }//GEN-LAST:event_jButtonAdicionarMaterialMouseExited

    private void jButtonCancelarEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarEditarActionPerformed
        Object[] options = {"NÃO","SIM"};
        int resposta = JOptionPane.showOptionDialog(getParent(),
                "Deseja cancelar a edição?",
                "Alerta", 
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, options, null);
        
        if(resposta != 0){
            modoEditar = false;
            jLabelEditarEntrada.setVisible(false);
            jTextNotaFiscal.setEditable(true);
            jTextMaterial.setEditable(true);
            jButtonCancelarEditar.setVisible(false);
            limparCamposEntrada();
        }
    }//GEN-LAST:event_jButtonCancelarEditarActionPerformed

    private void jButtonCancelarEditarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonCancelarEditarMouseEntered
        jButtonCancelarEditar.setText("");
        jButtonCancelarEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/icons8_delete_sign_32.png")));
        jButtonCancelarEditar.setBackground(Color.RED);
    }//GEN-LAST:event_jButtonCancelarEditarMouseEntered

    private void jButtonCancelarEditarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonCancelarEditarMouseExited
        jButtonCancelarEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        jButtonCancelarEditar.setText("CANCELAR");
        jButtonCancelarEditar.setBackground(new Color(240,240,240));
    }//GEN-LAST:event_jButtonCancelarEditarMouseExited

    private void jButtonPesquisaUnidadeMedidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPesquisaUnidadeMedidaActionPerformed
        TelaVisualizarUnidadeMedida unidade = new TelaVisualizarUnidadeMedida();
        unidade.setVisible(true);
    }//GEN-LAST:event_jButtonPesquisaUnidadeMedidaActionPerformed

    private void jButtonAdicionarCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarCategoriaActionPerformed
        TelaAdicionarCategoria categoria = new TelaAdicionarCategoria();
        categoria.setVisible(true);
    }//GEN-LAST:event_jButtonAdicionarCategoriaActionPerformed

    private void jButtonAdicionarUnidadeMedidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarUnidadeMedidaActionPerformed
        TelaAdicionarUnidade unidade = new TelaAdicionarUnidade();
        unidade.setVisible(true);
    }//GEN-LAST:event_jButtonAdicionarUnidadeMedidaActionPerformed

    private void jButtonAdicionarCategoriaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAdicionarCategoriaMouseEntered
        jButtonAdicionarCategoria.setBackground(new Color(115,140,0));
        jLabelCategorias.setForeground(new Color(115,140,0));
    }//GEN-LAST:event_jButtonAdicionarCategoriaMouseEntered

    private void jButtonAdicionarCategoriaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAdicionarCategoriaMouseExited
        jButtonAdicionarCategoria.setBackground(new Color(240,240,240));
        jLabelCategorias.setForeground(Color.BLACK);
    }//GEN-LAST:event_jButtonAdicionarCategoriaMouseExited

    private void jButtonPesquisaCategoriasMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonPesquisaCategoriasMouseEntered
        jButtonPesquisaCategorias.setBackground(new Color(115,140,0));
        jLabelCategorias.setForeground(new Color(115,140,0));
    }//GEN-LAST:event_jButtonPesquisaCategoriasMouseEntered

    private void jButtonAdicionarUnidadeMedidaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAdicionarUnidadeMedidaMouseEntered
        jButtonAdicionarUnidadeMedida.setBackground(new Color(115,140,0));
        jLabelUnidadeMedida.setForeground(new Color(115,140,0));
    }//GEN-LAST:event_jButtonAdicionarUnidadeMedidaMouseEntered

    private void jButtonAdicionarUnidadeMedidaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAdicionarUnidadeMedidaMouseExited
        jButtonAdicionarUnidadeMedida.setBackground(new Color(240,240,240));
        jLabelUnidadeMedida.setForeground(Color.BLACK);
    }//GEN-LAST:event_jButtonAdicionarUnidadeMedidaMouseExited

    private void jButtonPesquisaCategoriasMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonPesquisaCategoriasMouseExited
        jButtonPesquisaCategorias.setBackground(new Color(240,240,240));
        jLabelCategorias.setForeground(Color.BLACK);
    }//GEN-LAST:event_jButtonPesquisaCategoriasMouseExited

    private void jButtonPesquisaUnidadeMedidaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonPesquisaUnidadeMedidaMouseEntered
        jButtonPesquisaUnidadeMedida.setBackground(new Color(115,140,0));
        jLabelUnidadeMedida.setForeground(new Color(115,140,0));
    }//GEN-LAST:event_jButtonPesquisaUnidadeMedidaMouseEntered

    private void jButtonPesquisaUnidadeMedidaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonPesquisaUnidadeMedidaMouseExited
        jButtonPesquisaUnidadeMedida.setBackground(new Color(240,240,240));
        jLabelUnidadeMedida.setForeground(Color.BLACK);
    }//GEN-LAST:event_jButtonPesquisaUnidadeMedidaMouseExited

    private void jButtonPesquisaMateriaisMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonPesquisaMateriaisMouseEntered
        jButtonPesquisaMateriais.setBackground(new Color(115,140,0));
        jLabelMaterialMais.setForeground(new Color(115,140,0));
    }//GEN-LAST:event_jButtonPesquisaMateriaisMouseEntered

    private void jButtonPesquisaMateriaisMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonPesquisaMateriaisMouseExited
        jButtonPesquisaMateriais.setBackground(new Color(240,240,240));
        jLabelMaterialMais.setForeground(Color.BLACK);
    }//GEN-LAST:event_jButtonPesquisaMateriaisMouseExited

    private void jButtonPesquisaCategoriasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPesquisaCategoriasActionPerformed
        TelaVisualizarCategorias categorias = new TelaVisualizarCategorias();
        categorias.setVisible(true);
    }//GEN-LAST:event_jButtonPesquisaCategoriasActionPerformed

    private void jButtonPesquisaMateriaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPesquisaMateriaisActionPerformed
        TelaVisualizarMaterial telaMaterial = new TelaVisualizarMaterial();
        telaMaterial.setVisible(true);
    }//GEN-LAST:event_jButtonPesquisaMateriaisActionPerformed

    private void jTextCodigoDeBarrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextCodigoDeBarrasActionPerformed
        Enter = 1;
    }//GEN-LAST:event_jTextCodigoDeBarrasActionPerformed

    private void jTextCodigoDeBarrasKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextCodigoDeBarrasKeyReleased
        if(Enter != 0){
            jTextFornecedor.requestFocus();
            Enter = 0;
        }
    }//GEN-LAST:event_jTextCodigoDeBarrasKeyReleased

    private void jTextNotaFiscalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextNotaFiscalKeyReleased
        if(Enter != 0){
            jTextCodigoDeBarras.requestFocus();
            Enter = 0;
        }
    }//GEN-LAST:event_jTextNotaFiscalKeyReleased

    private void jFormattedDataInicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFormattedDataInicioActionPerformed
        Enter = 1;
    }//GEN-LAST:event_jFormattedDataInicioActionPerformed

    private void jFormattedDataInicioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jFormattedDataInicioKeyReleased
        if(Enter != 0){
            jFormattedDataFinal.requestFocus();
            Enter = 0;
        }
    }//GEN-LAST:event_jFormattedDataInicioKeyReleased

    private void jFormattedDataFinalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFormattedDataFinalActionPerformed
        Enter = 1;
    }//GEN-LAST:event_jFormattedDataFinalActionPerformed

    private void jFormattedDataFinalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jFormattedDataFinalKeyReleased
        if(Enter != 0){
            jButtonRelatorio.doClick();
            Enter = 0;
        }
    }//GEN-LAST:event_jFormattedDataFinalKeyReleased

    private void jButtonGerarRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGerarRelatorioActionPerformed
        jTabbedPrincipal.setSelectedComponent(jPanelRelatório);
    }//GEN-LAST:event_jButtonGerarRelatorioActionPerformed

    private void jButtonGerarRelatorioMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonGerarRelatorioMouseEntered
        jButtonGerarRelatorio.setBackground(new Color(115,140,0));
        jLabelGerarRelatorios.setForeground(new Color(115,140,0));
        jButtonGerarRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/pdf-2_26px.png")));
    }//GEN-LAST:event_jButtonGerarRelatorioMouseEntered

    private void jButtonGerarRelatorioMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonGerarRelatorioMouseExited
        jButtonGerarRelatorio.setBackground(new Color(240,240,240));
        jLabelGerarRelatorios.setForeground(Color.BLACK);
        jButtonGerarRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/pdf-2_balck_26px.png")));
    }//GEN-LAST:event_jButtonGerarRelatorioMouseExited

    private void jButtonAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarActionPerformed
        if(camposSaidaVazios())
            JOptionPane.showMessageDialog(getParent(), "Preencha todos os campos."," ALERTA",JOptionPane.WARNING_MESSAGE);
        else{
            Material material;
            Solicitante solicitante = new Solicitante();
            solicitante.setId(0);
            
            if(!jTextSolicitante.getText().trim().isEmpty())
                solicitante = (Solicitante) MODELOsolicitante.getElementAt(jListSolicitante.getSelectedIndex());
            
            material = (Material) MODELOdescricao.getElementAt(jListDescricao.getSelectedIndex());
            Saida saida = new Saida(Integer.parseInt(jSpinnerQuantidadeRetirada.getValue().toString().trim()),jFormattedDataSaida.getText().trim(),solicitante,material);
            
            addTabela(saida);
            
            saida.setData(formatarData(saida.getData()));
            lista.add(saida);
            jSpinnerQuantidadeRetirada.setValue(0);
            jButtonAdicionar.setEnabled(false);
            jButtonSaida.setEnabled(true);
        }
    }//GEN-LAST:event_jButtonAdicionarActionPerformed

    private void jButtonAdicionarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAdicionarMouseEntered
        if(jButtonAdicionar.isEnabled()){
            jButtonAdicionar.setText("");
            jButtonAdicionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/plus_math_26px.png")));
            jButtonAdicionar.setBackground(new Color(255,153,0));
        }
        
        //jButtonAdicionar.setForeground(Color.WHITE);
    }//GEN-LAST:event_jButtonAdicionarMouseEntered

    private void jButtonAdicionarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAdicionarMouseExited
        if(jButtonAdicionar.isEnabled()){
            jButtonAdicionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
            jButtonAdicionar.setText("ADICIONAR");
            jButtonAdicionar.setBackground(new Color(240,240,240));
            jButtonAdicionar.setForeground(Color.BLACK);
        }
    }//GEN-LAST:event_jButtonAdicionarMouseExited

    private void jSpinnerQuantidadeRetiradaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSpinnerQuantidadeRetiradaStateChanged
        int qtd = Integer.parseInt(jSpinnerQuantidadeRetirada.getValue().toString());
        
        if(qtd < 0)
            jSpinnerQuantidadeRetirada.setValue(0);
        else if(qtd > 0 && !jTextDescricao.getText().trim().isEmpty())
            verificarQuantidadeTotalDisponivel();
        
    }//GEN-LAST:event_jSpinnerQuantidadeRetiradaStateChanged

    private void jButtonAdicionarSolicitanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdicionarSolicitanteActionPerformed
        novoSolicitante();
    }//GEN-LAST:event_jButtonAdicionarSolicitanteActionPerformed

    private void jButtonAdicionarSolicitanteMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAdicionarSolicitanteMouseExited
        jButtonAdicionarSolicitante.setBackground(new Color(240,240,240));
        jButtonAdicionarSolicitante.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/plus_math_black_26px.png")));
    }//GEN-LAST:event_jButtonAdicionarSolicitanteMouseExited

    private void jButtonAdicionarSolicitanteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAdicionarSolicitanteMouseEntered
        jButtonAdicionarSolicitante.setBackground(new Color(115,140,29));
        jButtonAdicionarSolicitante.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/plus_math_26px.png")));
    }//GEN-LAST:event_jButtonAdicionarSolicitanteMouseEntered

    private void jTextSolicitanteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextSolicitanteKeyReleased
        if (Enter == 0) {
            listaPesquisaNome(jTextSolicitante.getText().trim());
        } else {
            con = ConnectionFactory.getConnection();
            try {
                int Linha = jListSolicitante.getSelectedIndex() + 2;

                stmt = (PreparedStatement) con.prepareStatement("SELECT * FROM solicitante "
                    + "WHERE nome like '" + jTextSolicitante.getText() + "%' ORDER BY nome LIMIT " + Linha + ";");
                rs = stmt.executeQuery();
                rs.first();

                jTextSolicitante.setText(rs.getString("nome"));
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Erro ao selecionar nome\n" + ex);
            } finally {
                ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
            }
            Enter = 0;
            jTextDescricao.requestFocus();
        }
    }//GEN-LAST:event_jTextSolicitanteKeyReleased

    private void jTextSolicitanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextSolicitanteActionPerformed
        jListSolicitante.setVisible(false);
        Enter = 1;
    }//GEN-LAST:event_jTextSolicitanteActionPerformed

    private void jListSolicitanteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jListSolicitanteMouseClicked
        jTextSolicitante.setText(String.valueOf(MODELOsolicitante.getElementAt(jListSolicitante.getSelectedIndex())));
        jListSolicitante.setVisible(false);
    }//GEN-LAST:event_jListSolicitanteMouseClicked

    private void jSpinnerQuantidadeStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSpinnerQuantidadeStateChanged
        if(Integer.parseInt(jSpinnerQuantidade.getValue().toString()) < 0){
            jSpinnerQuantidade.setValue(0);
        } else if(Integer.parseInt(jSpinnerQuantidade.getValue().toString()) == 0){
            if(jButtonCadastrar.isEnabled()){
                jButtonCadastrar.setEnabled(false);
                jButtonAdicionar.setEnabled(false);
            }
        } else{
            if(!jButtonCadastrar.isEnabled())
                jButtonCadastrar.setEnabled(true);
        }
    }//GEN-LAST:event_jSpinnerQuantidadeStateChanged

    private void jMenuConfiguracoesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuConfiguracoesActionPerformed
        TelaAdministrador telaAdm = new TelaAdministrador();
        telaAdm.setAdministrador(adm);
        telaAdm.setVisible(true);
    }//GEN-LAST:event_jMenuConfiguracoesActionPerformed

    private void jMenuItemAdministradorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemAdministradorActionPerformed
        TelaAdministrador telaAdm = new TelaAdministrador();
        telaAdm.setAdministrador(adm);
        telaAdm.setVisible(true);
    }//GEN-LAST:event_jMenuItemAdministradorActionPerformed

    private void jMenu3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MouseClicked
        TelaSobre sobre = new TelaSobre();
        sobre.setVisible(true);
    }//GEN-LAST:event_jMenu3MouseClicked
    
    public void irParaEntrada(){
        jTabbedPrincipal.setSelectedComponent(jPanelEntrada);
    }
    
    public void novoFornecedor() {
        TelaAdicionarFornecedor novoFornecedor = new TelaAdicionarFornecedor();
        novoFornecedor.setVisible(true);
    }

    public void novoMaterial() {
        TelaAdicionarMaterial novoMaterial = new TelaAdicionarMaterial();
        novoMaterial.setVisible(true);
    }
    
    public void novoSolicitante(){
        TelaAdicionarSolicitante novoSolicitante = new TelaAdicionarSolicitante();
        novoSolicitante.setVisible(true);
    }
    
    public void visualizarEstoque(){
        TelaVisualizarEstoque visualizarEstoque = new TelaVisualizarEstoque();
        visualizarEstoque.setVisible(true);
    }
    
    public void visualizarFornecedores(){
        TelaVisualizarFornecedores visualizarFornecedores = new TelaVisualizarFornecedores();
        visualizarFornecedores.setVisible(true);
    }
    
    public void visualizarSolicitantes(){
        TelaVisualizarSolicitantes visualizarSolicitantes = new TelaVisualizarSolicitantes();
        visualizarSolicitantes.setVisible(true);
    }
    
    public void visualizarEntradas(){
        TelaVisualizarEntradas visualizarEntradas = new TelaVisualizarEntradas(this);
        visualizarEntradas.setVisible(true);
    }
    
    public void visualizarSaidas(){
        TelaVisualizarSaidas visualizarSaidas = new TelaVisualizarSaidas();
        visualizarSaidas.setVisible(true);
    }
    
    public void visualizarAlerta(String msgm){
        TelaAlerta alerta = new TelaAlerta(msgm);
        alerta.setVisible(true);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.Box.Filler filler1;
    private javax.swing.Box.Filler filler2;
    private javax.swing.Box.Filler filler3;
    private javax.swing.JButton jButtonAdicionar;
    private javax.swing.JButton jButtonAdicionarCategoria;
    private javax.swing.JButton jButtonAdicionarFornecedor;
    private javax.swing.JButton jButtonAdicionarMaterial;
    private javax.swing.JButton jButtonAdicionarSolicitante;
    private javax.swing.JButton jButtonAdicionarUnidadeMedida;
    private javax.swing.JButton jButtonAlerta;
    private javax.swing.JButton jButtonCadastrar;
    private javax.swing.JButton jButtonCancelarEditar;
    private javax.swing.JButton jButtonEmitirMaterial;
    private javax.swing.JButton jButtonEmitirSetor;
    private javax.swing.JButton jButtonEntradaMais;
    private javax.swing.JButton jButtonEntradasVisualizar;
    private javax.swing.JButton jButtonEstoqueVisualizar;
    private javax.swing.JButton jButtonFornecedorMais;
    private javax.swing.JButton jButtonFornecedorVisualizar;
    private javax.swing.JButton jButtonGerarRelatorio;
    private javax.swing.JButton jButtonMaterialMais;
    private javax.swing.JButton jButtonPesquisaCategorias;
    private javax.swing.JButton jButtonPesquisaMateriais;
    private javax.swing.JButton jButtonPesquisaUnidadeMedida;
    private javax.swing.JButton jButtonRelatorio;
    private javax.swing.JButton jButtonSaida;
    private javax.swing.JButton jButtonSaidaMais;
    private javax.swing.JButton jButtonSaidasVisualizar;
    private javax.swing.JButton jButtonSolicitantesMais;
    private javax.swing.JButton jButtonSolicitantesVisualizar;
    private javax.swing.JCheckBox jCheckBoxDataMaterial;
    private javax.swing.JCheckBox jCheckBoxDataSetor;
    private javax.swing.JCheckBox jCheckBoxEntrada;
    private javax.swing.JCheckBox jCheckBoxEntradaMaterial;
    private javax.swing.JCheckBox jCheckBoxSaida;
    private javax.swing.JCheckBox jCheckBoxSaidaMaterial;
    private javax.swing.JComboBox<String> jComboBoxAquisicao;
    private javax.swing.JComboBox<String> jComboBoxSetor;
    private javax.swing.JFormattedTextField jFormattedDataFinal;
    private javax.swing.JFormattedTextField jFormattedDataFinalMaterial;
    private javax.swing.JFormattedTextField jFormattedDataFinalSetor;
    private javax.swing.JFormattedTextField jFormattedDataInicio;
    private javax.swing.JFormattedTextField jFormattedDataInicioMaterial;
    private javax.swing.JFormattedTextField jFormattedDataInicioSetor;
    private javax.swing.JFormattedTextField jFormattedDataSaida;
    private javax.swing.JFormattedTextField jFormattedTextData;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelAlertas;
    private javax.swing.JLabel jLabelCategorias;
    private javax.swing.JLabel jLabelEditarEntrada;
    private javax.swing.JLabel jLabelEntradas;
    private javax.swing.JLabel jLabelEstoque;
    private javax.swing.JLabel jLabelFornecedor;
    private javax.swing.JLabel jLabelGerarRelatorios;
    private javax.swing.JLabel jLabelMaterialMais;
    private javax.swing.JLabel jLabelNomeAdministrador;
    private javax.swing.JLabel jLabelQtdAlertas;
    private javax.swing.JLabel jLabelSaidas;
    private javax.swing.JLabel jLabelSolicitantes;
    private javax.swing.JLabel jLabelTituloEntrada;
    private javax.swing.JLabel jLabelUnidadeMedida;
    private javax.swing.JLayeredPane jLayeredPaneSaida;
    private javax.swing.JList<String> jListDescricao;
    private javax.swing.JList<String> jListFornecedor;
    private javax.swing.JList<String> jListMaterial;
    private javax.swing.JList<String> jListRelatorioMaterial;
    private javax.swing.JList<String> jListSolicitante;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenu jMenuConfiguracoes;
    private javax.swing.JMenuItem jMenuItemAdministrador;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanelBoxEntrada;
    private javax.swing.JPanel jPanelBoxHome;
    private javax.swing.JPanel jPanelBoxRelatorio;
    private javax.swing.JPanel jPanelBoxSaida;
    private javax.swing.JPanel jPanelEntrada;
    private javax.swing.JPanel jPanelHome;
    private javax.swing.JPanel jPanelRelatorio;
    private javax.swing.JPanel jPanelRelatório;
    private javax.swing.JRadioButton jRadioButtonTodos;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSpinner jSpinnerQuantidade;
    private javax.swing.JSpinner jSpinnerQuantidadeRetirada;
    private javax.swing.JTabbedPane jTabbedPrincipal;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTableCadastroMaterial;
    private javax.swing.JTable jTableSaidaMaterial;
    private javax.swing.JTextField jTextCodigoDeBarras;
    private javax.swing.JTextField jTextDescricao;
    private javax.swing.JTextField jTextFieldUnidadeMedida;
    private javax.swing.JTextField jTextFornecedor;
    private javax.swing.JTextField jTextMaterial;
    private javax.swing.JTextField jTextNotaFiscal;
    private javax.swing.JTextField jTextRelatorioMaterial;
    private javax.swing.JTextField jTextSolicitante;
    // End of variables declaration//GEN-END:variables

    public void preencherTabelaEntrada(Entrada entrada) {
        DefaultTableModel tabelaMateriaisEntrada = (DefaultTableModel) jTableCadastroMaterial.getModel();

        tabelaMateriaisEntrada.addRow(new String[]{
            entrada.getNotaFiscal(),
            entrada.getMaterial_idMaterial().getDescricao(),
            Integer.toString(entrada.getQuantidade()),
            entrada.getData(),
            entrada.getTipoAquisicaoId().getDescricao(),
            entrada.getFornecedorCnpj().getNome()
        });
    }
    /*
    public void preencherTabelaSaida(Saida saida, Material material){
        DefaultTableModel tabelaMateriaisSaida = (DefaultTableModel) jTableSaidaMaterial.getModel();
        
        tabelaMateriaisSaida.addRow(new String[]{
            material.getDescricao(),
            Integer.toString(saida.getQuantidade()),
            saida.getSolicitante_id().getNome(),
            saida.getData()
        });
    }*/
    
    public void limparCamposEntrada(){
        jTextCodigoDeBarras.setText("");
        jTextNotaFiscal.setText("");
        jTextFornecedor.setText("");
        jTextMaterial.setText("");
        jSpinnerQuantidade.setValue(0);
    }
    
    public void limparCamposSaida(){
        jTextSolicitante.setText("");
        jTextDescricao.setText("");
        jSpinnerQuantidadeRetirada.setValue(0);
    }
    
    public void checkBoxEntradaMaterial(Document document, Paragraph paragrafo, String entradaMaterial, String qtdEntrada) throws DocumentException{
        float[] columnWidths = {6,10,10,4,10,10,7};
        PdfPTable tabela = new PdfPTable(columnWidths);
        tabela.setWidthPercentage(100);
        tabela.getDefaultCell().setUseAscender(true);
        tabela.getDefaultCell().setUseDescender(true);

        Font f = new Font(Font.FontFamily.COURIER, 8, Font.NORMAL, GrayColor.WHITE);
        PdfPCell cabecalho = new PdfPCell(new Paragraph("RELATÓRIO DE ENTRADAS DE MATERIAIS",new Font(Font.FontFamily.COURIER, 11, Font.BOLD)));
        cabecalho.setHorizontalAlignment(ALIGN_CENTER);
        cabecalho.setPaddingBottom(10);
        cabecalho.setBorder(PdfPCell.NO_BORDER);
        cabecalho.setColspan(7);
        tabela.addCell(cabecalho);

        PdfPCell cel1 = new PdfPCell(new Phrase("DATA",f));
        cel1.setHorizontalAlignment(ALIGN_CENTER);
        cel1.setBackgroundColor(GrayColor.DARK_GRAY);
        cel1.setPaddingTop(5);
        cel1.setPaddingBottom(5);

        PdfPCell cel2 = new PdfPCell(new Phrase("NOTA FISCAL",f));
        cel2.setHorizontalAlignment(ALIGN_CENTER);
        cel2.setBackgroundColor(GrayColor.DARK_GRAY);
        cel2.setPaddingTop(5);
        cel2.setPaddingBottom(5);

        PdfPCell cel3 = new PdfPCell(new Phrase("MATERIAL",f));
        cel3.setHorizontalAlignment(ALIGN_CENTER);
        cel3.setBackgroundColor(GrayColor.DARK_GRAY);
        cel3.setPaddingTop(5);
        cel3.setPaddingBottom(5);

        PdfPCell cel4 = new PdfPCell(new Phrase("QTD",f));
        cel4.setHorizontalAlignment(ALIGN_CENTER);
        cel4.setBackgroundColor(GrayColor.DARK_GRAY);
        cel4.setPaddingTop(5);
        cel4.setPaddingBottom(5);

        PdfPCell cel5 = new PdfPCell(new Phrase("FORNECEDOR",f));
        cel5.setHorizontalAlignment(ALIGN_CENTER);
        cel5.setBackgroundColor(GrayColor.DARK_GRAY);
        cel5.setPaddingTop(5);
        cel5.setPaddingBottom(5);

        PdfPCell cel6 = new PdfPCell(new Phrase("CNPJ",f));
        cel6.setHorizontalAlignment(ALIGN_CENTER);
        cel6.setBackgroundColor(GrayColor.DARK_GRAY);
        cel6.setPaddingTop(5);
        cel6.setPaddingBottom(5);

        PdfPCell cel7 = new PdfPCell(new Phrase("TIPO AQUISIÇÃO",f));
        cel7.setHorizontalAlignment(ALIGN_CENTER);
        cel7.setBackgroundColor(GrayColor.DARK_GRAY);
        cel7.setPaddingTop(5);
        cel7.setPaddingBottom(5);

        tabela.addCell(cel1);
        tabela.addCell(cel2);
        tabela.addCell(cel3);
        tabela.addCell(cel4);
        tabela.addCell(cel5);
        tabela.addCell(cel6);
        tabela.addCell(cel7);

        con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            stmt = (PreparedStatement) con.prepareStatement(entradaMaterial);
            rs = stmt.executeQuery();

            tabela.getDefaultCell().setHorizontalAlignment(ALIGN_CENTER);
            tabela.getDefaultCell().setPaddingTop(5);
            Font g = new Font(Font.FontFamily.COURIER, 10, Font.NORMAL, GrayColor.BLACK);

            while(rs.next()){
                tabela.addCell(new Phrase(rs.getString("entrada.dataEntrada"), g));
                tabela.addCell(new Phrase(rs.getString("entrada.nNotaFiscal"), g));
                tabela.addCell(new Phrase(rs.getString("material.descricao"), g));
                tabela.addCell(new Phrase(rs.getString("entrada.quantidade"), g));
                tabela.addCell(new Phrase(rs.getString("fornecedor.nome"), g));
                tabela.addCell(new Phrase(rs.getString("fornecedor.cnpj"), g));
                tabela.addCell(new Phrase(rs.getString("tipoaquisicao.descricao"), g));
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao consultar banco\n"+ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }

        document.add(tabela);
        paragrafo = new Paragraph(" ");
        document.add(paragrafo);

        con = ConnectionFactory.getConnection();

        try {
            stmt = (PreparedStatement) con.prepareStatement(qtdEntrada);
            rs = stmt.executeQuery();

            if(rs.next()){
                paragrafo = new Paragraph("Quantidade de entradas: "+rs.getInt("TOTAL"), new Font(Font.FontFamily.COURIER, 11, Font.BOLD));
                document.add(paragrafo);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao exibir total\n"+ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }
        
        paragrafo = new Paragraph(" ");
        document.add(paragrafo);
    }
    
    public void checkBoxSaidaMaterial(Document document, Paragraph paragrafo, String saidaMaterial, String qtdSaida) throws DocumentException{
        float[] columnWidths = {6,10,4,10,10};
        PdfPTable tabela = new PdfPTable(columnWidths);
        tabela.setWidthPercentage(100);
        tabela.getDefaultCell().setUseAscender(true);
        tabela.getDefaultCell().setUseDescender(true);

        Font f = new Font(Font.FontFamily.COURIER, 8, Font.NORMAL, GrayColor.WHITE);
        PdfPCell cabecalho = new PdfPCell(new Paragraph("RELATÓRIO DE SAÍDAS DE MATERIAIS",new Font(Font.FontFamily.COURIER, 11, Font.BOLD)));
        cabecalho.setHorizontalAlignment(ALIGN_CENTER);
        cabecalho.setPaddingBottom(10);
        cabecalho.setBorder(PdfPCell.NO_BORDER);
        cabecalho.setColspan(7);
        tabela.addCell(cabecalho);

        PdfPCell cel1 = new PdfPCell(new Phrase("DATA",f));
        cel1.setHorizontalAlignment(ALIGN_CENTER);
        cel1.setBackgroundColor(GrayColor.DARK_GRAY);
        cel1.setPaddingTop(5);
        cel1.setPaddingBottom(5);

        PdfPCell cel2 = new PdfPCell(new Phrase("MATERIAL",f));
        cel2.setHorizontalAlignment(ALIGN_CENTER);
        cel2.setBackgroundColor(GrayColor.DARK_GRAY);
        cel2.setPaddingTop(5);
        cel2.setPaddingBottom(5);

        PdfPCell cel3 = new PdfPCell(new Phrase("QTD",f));
        cel3.setHorizontalAlignment(ALIGN_CENTER);
        cel3.setBackgroundColor(GrayColor.DARK_GRAY);
        cel3.setPaddingTop(5);
        cel3.setPaddingBottom(5);

        PdfPCell cel4 = new PdfPCell(new Phrase("SOLICITANTE",f));
        cel4.setHorizontalAlignment(ALIGN_CENTER);
        cel4.setBackgroundColor(GrayColor.DARK_GRAY);
        cel4.setPaddingTop(5);
        cel4.setPaddingBottom(5);

        PdfPCell cel5 = new PdfPCell(new Phrase("SETOR",f));
        cel5.setHorizontalAlignment(ALIGN_CENTER);
        cel5.setBackgroundColor(GrayColor.DARK_GRAY);
        cel5.setPaddingTop(5);
        cel5.setPaddingBottom(5);

        tabela.addCell(cel1);
        tabela.addCell(cel2);
        tabela.addCell(cel3);
        tabela.addCell(cel4);
        tabela.addCell(cel5);

        con = ConnectionFactory.getConnection();

        try {
            stmt = (PreparedStatement) con.prepareStatement(saidaMaterial);
            rs = stmt.executeQuery();

            tabela.getDefaultCell().setHorizontalAlignment(ALIGN_CENTER);
            tabela.getDefaultCell().setPaddingTop(5);
            Font g = new Font(Font.FontFamily.COURIER, 10, Font.NORMAL, GrayColor.BLACK);

            while(rs.next()){
                tabela.addCell(new Phrase(rs.getString("saida.dataSaida"), g));
                tabela.addCell(new Phrase(rs.getString("material.descricao"), g));
                tabela.addCell(new Phrase(rs.getString("saida.quantidade"), g));
                tabela.addCell(new Phrase(rs.getString("solicitante.nome"), g));
                tabela.addCell(new Phrase(rs.getString("solicitante.setor"), g));
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao consultar banco\n"+ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }

        document.add(tabela);
        paragrafo = new Paragraph(" ");
        document.add(paragrafo);

        con = ConnectionFactory.getConnection();

        try {
            stmt = (PreparedStatement) con.prepareStatement(qtdSaida);
            rs = stmt.executeQuery();

            if(rs.next()){
                paragrafo = new Paragraph("Quantidade de saídas: "+rs.getInt("TOTAL"), new Font(Font.FontFamily.COURIER, 11, Font.BOLD));
                document.add(paragrafo);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"Erro ao exibir total\n"+ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con,stmt,rs);
        }
    }
    
    public int alertaQuantidadeMinima(Saida saida){
        con = ConnectionFactory.getConnection();
        stmt = null;
        rs = null;
        
        try {
            stmt = (PreparedStatement) con.prepareStatement("SELECT estoque.quantidade, material.qtd_minima FROM estoque INNER JOIN material "
                    + "ON estoque.material_idMaterial = material.idMaterial WHERE material.idMaterial = \""+saida.getMaterial_idMaterial().getIdMaterial()+"\";");
            rs = stmt.executeQuery();
            rs.first();
            
            int qtdEstoque = rs.getInt("estoque.quantidade");
            int qtdMinima = rs.getInt("material.qtd_minima");
            
            if(qtdEstoque == 0){
                visualizarAlerta("Estoque vazio! Não será possível efetuar saída de "+saida.getMaterial_idMaterial().getDescricao()+".");
                return 1;
            } else if((qtdEstoque - saida.getQuantidade()) == qtdMinima){
                visualizarAlerta("Limite mínimo de "+saida.getMaterial_idMaterial().getDescricao()+" atingido!");
                return 0;
            } else if(saida.getQuantidade() < qtdMinima && saida.getQuantidade() > 0){
                visualizarAlerta(saida.getMaterial_idMaterial().getDescricao()+" já está abaixo da quantidade mínima.");
                return 0;
            } else if(saida.getQuantidade() > qtdEstoque){
                visualizarAlerta("Quantidade de "+saida.getMaterial_idMaterial().getDescricao()+" excedente ao que possui no estoque!");
                return 1;
            } else{
                return 0;
            }
        } catch (SQLException ex) {
            System.out.println("Erro ao verificar quantidade mínima.\n"+ex);
        } finally{
            ConnectionFactory.closeConnection((com.mysql.jdbc.Connection) con, stmt, rs);
        }
        return 0;
    }
    
    public void editarEntrada(Entrada entrada){
        modoEditar = true;
        
        if(!jSpinnerQuantidade.isEnabled() || !jButtonCadastrar.isEnabled()){
            jSpinnerQuantidade.setEnabled(true);
            jButtonCadastrar.setEnabled(true);
        }
        
        jButtonCancelarEditar.setVisible(true);
        jLabelEditarEntrada.setVisible(true);
        jTextNotaFiscal.setEditable(false);
        jTextMaterial.setEditable(false);
        aux = entrada.getQuantidade();
        jTextNotaFiscal.setText(entrada.getNotaFiscal());
        jTextMaterial.setText(entrada.getMaterial_idMaterial().getDescricao());
        jComboBoxAquisicao.setSelectedItem(entrada.getTipoAquisicaoId().getDescricao());
        jTextFornecedor.setText(entrada.getFornecedorCnpj().getNome());
        jSpinnerQuantidade.setValue(entrada.getQuantidade());
    }

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("../imagens/logo/IconeSemBackground.png")));
    }
}