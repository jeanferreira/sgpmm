

package View;
import java.awt.*;
import javax.swing.*;

/**
 *
 * @author Micaelle Queiroz
 */
public class BackgPanel extends javax.swing.JPanel {

    /**s
     * Creates new form BackgPanel
     */
    public BackgPanel() {
        initComponents();
        this.setSize(1930, 1050);
    }
@Override
    public void paintComponent(Graphics g){
        Dimension tamanho = getSize();
        ImageIcon imageBack = new ImageIcon(new ImageIcon(getClass().getResource("/imagens/9.jpg")).getImage());
        g.drawImage(imageBack.getImage(), 0, 0, tamanho.width,tamanho.height,null);
        setOpaque(false);
        super.paintComponent(g);

}
    
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
