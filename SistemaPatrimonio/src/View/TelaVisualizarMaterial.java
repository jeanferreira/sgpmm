/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.bean.Material;
import Model.dao.MaterialDAO;
import java.util.ArrayList;
import java.util.List;
import Utils.ModelTable;
import java.awt.Color;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Jean Ferreira
 */
public class TelaVisualizarMaterial extends javax.swing.JFrame {
    private String[] colunas = {"DESCRIÇÃO","QTD MÍNIMA","UNIDADE","CATEGORIA"};
    private List<Material> material = new ArrayList<>();
    private ArrayList dados;
    private String ASPASDUPLAS = "\"";
    private String CONTRABARRAASPASDUPLAS = "\\\"";
    /**
     * Creates new form TelaVisualizarMaterial
     */
    public TelaVisualizarMaterial() {
        initComponents();
        inicializarTabela();
    }

    public void inicializarTabela(){
        jTableMateriais.removeAll();
        
        MaterialDAO materialDAO = new MaterialDAO();
        jLabelQuantidade.setText("Quantidade de materiais: "+Integer.toString(materialDAO.pegarQuantidade()));
        material = materialDAO.selecionarMateriais();
        construirTabela(material);
    }
    
    public void construirTabela(List<Material> material){
        dados = new ArrayList();
        
        for(Material tabela : material){
            dados.add(new Object[]{
                tabela.getDescricao(),
                tabela.getQtd_minima(),
                tabela.getCategoriaId().getNome(),
                tabela.getIdUnidadeMedida().getTipo()
            });
        }
        
        ModelTable model = new ModelTable(dados, colunas);
        
        jTableMateriais.setModel(model);
        jTableMateriais.setAutoResizeMode(jTableMateriais.AUTO_RESIZE_ALL_COLUMNS);
        
        jTableMateriais.getColumnModel().getColumn(0).setPreferredWidth(220);
        jTableMateriais.getColumnModel().getColumn(1).setPreferredWidth(10);
        jTableMateriais.getColumnModel().getColumn(2).setPreferredWidth(20);
        jTableMateriais.getColumnModel().getColumn(3).setPreferredWidth(20);
        jTableMateriais.getTableHeader().setResizingAllowed(true);
        jTableMateriais.getTableHeader().setReorderingAllowed(false);
        jTableMateriais.getSelectionModel().addListSelectionListener(selecionarLinha());
        
        jTableMateriais.repaint();
    }
    
    public ListSelectionListener selecionarLinha(){
        return new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent lse) {
                if(jTableMateriais.getSelectedRow() != 1){
                    String descricao = (String) jTableMateriais.getValueAt(jTableMateriais.getSelectedRow(), 0);
                    
                    Object[] options = {"EXCLUIR","EDITAR","CANCELAR"};
                    
                    int resposta = JOptionPane.showOptionDialog(null,
                            "Editar "+descricao,
                            "Deseja editar?",
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.QUESTION_MESSAGE, null, options, null);
                    
                    if(resposta == 0){
                        Object[] optionsExcluir = {"EXCLUIR","CANCELAR"};
                    
                        int res = JOptionPane.showOptionDialog(null,
                                "Tem certeza que deseja excluir "+descricao+" ?",
                                "ALERTA",
                                JOptionPane.DEFAULT_OPTION,
                                JOptionPane.QUESTION_MESSAGE, null, optionsExcluir, null);
                        
                        if(res == 0){
                            MaterialDAO materialDao = new MaterialDAO();
                            Material material = materialDao.selecionar(descricao.replace(ASPASDUPLAS, CONTRABARRAASPASDUPLAS));
                            
                            if(materialDao.verificarEstoque(material) != true){
                                materialDao.deletarMaterial(material);
                            } else{
                                JOptionPane.showMessageDialog(null,"Não será possível excluir este material!");
                            }
                        }
                    } else if(resposta == 1){
                        MaterialDAO materialDAO = new MaterialDAO();
                        Material material = materialDAO.selecionar(descricao.replace(ASPASDUPLAS, CONTRABARRAASPASDUPLAS));
                        
                        TelaAdicionarMaterial telaMaterial = new TelaAdicionarMaterial();
                        telaMaterial.setVisible(true);
                        telaMaterial.editarMaterial(material);
                    } else{
                        
                    }
                    jTableMateriais.clearSelection();
                }
            }
        };
    }
    
    public void atualizarTabela(){
        MaterialDAO materialDAO = new MaterialDAO();
        material = materialDAO.selecionarMateriais();
        
        construirTabela(material);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabelQuantidade = new javax.swing.JLabel();
        jButtonCancelar = new javax.swing.JButton();
        jButtonAtualizar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableMateriais = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Materiais");
        setUndecorated(true);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(115, 140, 0), 2));
        jPanel1.setFocusable(false);
        jPanel1.setPreferredSize(new java.awt.Dimension(1162, 725));

        jLabel1.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("MATERIAIS");

        jLabelQuantidade.setFont(new java.awt.Font("Century Gothic", 1, 18)); // NOI18N
        jLabelQuantidade.setForeground(new java.awt.Color(115, 140, 0));

        jButtonCancelar.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        jButtonCancelar.setText("FECHAR");
        jButtonCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonCancelarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonCancelarMouseExited(evt);
            }
        });
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        jButtonAtualizar.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        jButtonAtualizar.setText("ATUALIZAR");
        jButtonAtualizar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButtonAtualizarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButtonAtualizarMouseExited(evt);
            }
        });
        jButtonAtualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAtualizarActionPerformed(evt);
            }
        });

        jTableMateriais.setBackground(new java.awt.Color(115, 140, 0));
        jTableMateriais.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        jTableMateriais.setForeground(new java.awt.Color(255, 255, 255));
        jTableMateriais.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTableMateriais);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(98, 98, 98)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 370, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonAtualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 852, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(98, 98, 98))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jLabel1)
                .addGap(60, 60, 60)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonAtualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1052, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 694, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    private void jButtonCancelarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonCancelarMouseEntered
        jButtonCancelar.setText("");
        jButtonCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/icons8_delete_sign_32.png")));
        jButtonCancelar.setBackground(Color.RED);
    }//GEN-LAST:event_jButtonCancelarMouseEntered

    private void jButtonCancelarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonCancelarMouseExited
        jButtonCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        jButtonCancelar.setText("FECHAR");
        jButtonCancelar.setBackground(new Color(240,240,240));
    }//GEN-LAST:event_jButtonCancelarMouseExited

    private void jButtonAtualizarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAtualizarMouseEntered
        jButtonAtualizar.setText("");
        jButtonAtualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/botoes/icons8_synchronize_32.png")));
        jButtonAtualizar.setBackground(new Color(115,140,0));
    }//GEN-LAST:event_jButtonAtualizarMouseEntered

    private void jButtonAtualizarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonAtualizarMouseExited
        jButtonAtualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("")));
        jButtonAtualizar.setText("ATUALIZAR");
        jButtonAtualizar.setBackground(new Color(240,240,240));
    }//GEN-LAST:event_jButtonAtualizarMouseExited

    private void jButtonAtualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAtualizarActionPerformed
        atualizarTabela();
    }//GEN-LAST:event_jButtonAtualizarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaVisualizarMaterial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaVisualizarMaterial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaVisualizarMaterial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaVisualizarMaterial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaVisualizarMaterial().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAtualizar;
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelQuantidade;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableMateriais;
    // End of variables declaration//GEN-END:variables
}
