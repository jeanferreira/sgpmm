/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;


public class VariableRowHeightRenderer extends JTextArea implements TableCellRenderer {
    int sizeMax;
    public VariableRowHeightRenderer(int size) {
        super();
        this.sizeMax=size;
        //setOpaque(true);
        setLineWrap(true);
        setWrapStyleWord(true);
        setBackground(Color.GRAY);
        setForeground(Color.WHITE);
        setFont(new Font("Century Gothic",Font.BOLD,14));
       
        
        
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
        boolean hasFocus, int row, int column) {
        setText(value.toString());
        int rowNum=1;
        int size =value.toString().length();
        while(size>sizeMax){
            rowNum++;
            size-=sizeMax;
        }
        
        if(value.toString().length()>sizeMax){
            table.setRowHeight(row, (int)Math.round((getPreferredSize().height * rowNum*0.5)));
        }else{
             table.setRowHeight(row, (int)Math.round((getPreferredSize().height)));
        }
        table.setBackground(Color.GRAY);
        return this;
  }
     
}