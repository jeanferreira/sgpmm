package Utils;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public class ModelTable extends AbstractTableModel {
	
    private ArrayList linhas = null;
    private String[] colunas = null;
        
    public ModelTable(ArrayList linhas, String[] colunas) {
        this.linhas = linhas;
        this.colunas = colunas;
    }
    @Override
    public int getColumnCount() {
        return this.colunas.length;
    }
    @Override
    public int getRowCount() {
        return this.linhas.size();
    }
    @Override
    public String getColumnName(int column) {
        return this.colunas[column];
    }
    public Object getValueAt(int numLin,int numColum){
        Object[] linha = (Object[])this.linhas.get(numLin);
        return linha[numColum];    
    }
    
}