-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 22-Out-2018 às 19:59
-- Versão do servidor: 5.5.35
-- versão do PHP: 5.4.45-0+deb7u8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `sgpmm`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `administrador`
--

CREATE TABLE IF NOT EXISTS `administrador` (
  `siape` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `senha` varchar(15) NOT NULL,
  PRIMARY KEY (`siape`),
  KEY `siape` (`siape`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `administrador`
--

INSERT INTO `administrador` (`siape`, `nome`, `senha`) VALUES
(3073519, 'Monik de Castro Rodrigues', 'gpmm'),
(1234567, 'Laboratório 312', 'lab312');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `entrada`
--

CREATE TABLE IF NOT EXISTS `entrada` (
  `nNotaFiscal` varchar(100) NOT NULL DEFAULT '',
  `codBarras` varchar(45) DEFAULT NULL,
  `quantidade` int(11) NOT NULL,
  `dataEntrada` varchar(10) NOT NULL,
  `tipoAquisicao_idtipoAquisicao` int(11) DEFAULT NULL,
  `fornecedor_cnpj` varchar(18) DEFAULT NULL,
  `material_idMaterial` int(11) NOT NULL,
  PRIMARY KEY (`nNotaFiscal`),
  KEY `fk_entrada_tipoAquisicao1_idx` (`tipoAquisicao_idtipoAquisicao`),
  KEY `fk_entrada_fornecedor1_idx` (`fornecedor_cnpj`),
  KEY `fk_entrada_material1_idx` (`material_idMaterial`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `estoque`
--

CREATE TABLE IF NOT EXISTS `estoque` (
  `idEstoque` int(11) NOT NULL AUTO_INCREMENT,
  `quantidade` int(11) NOT NULL,
  `entrada_nNotaFiscal` varchar(100) NOT NULL,
  `material_idMaterial` varchar(45) NOT NULL,
  `saida_idSaida` int(11) DEFAULT NULL,
  `categoria_idCategoria` int(11) NOT NULL,
  `unidadeMedida_idunidadeMedida` int(11) NOT NULL,
  PRIMARY KEY (`idEstoque`),
  KEY `fk_estoque_material1_idx` (`material_idMaterial`),
  KEY `fk_estoque_saida1` (`saida_idSaida`),
  KEY `fk_estoque_categoria1` (`categoria_idCategoria`),
  KEY `fk_estoque_unidadeMedida1` (`unidadeMedida_idunidadeMedida`),
  KEY `entrada_nNotaFiscal` (`entrada_nNotaFiscal`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fornecedor`
--

CREATE TABLE IF NOT EXISTS `fornecedor` (
  `cnpj` varchar(18) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `telefone` varchar(17) DEFAULT NULL,
  PRIMARY KEY (`cnpj`),
  UNIQUE KEY `cnpj_UNIQUE` (`cnpj`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `material`
--

CREATE TABLE IF NOT EXISTS `material` (
  `idMaterial` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) NOT NULL,
  `qtd_minima` int(11) NOT NULL,
  `unidadeMedida_idunidadeMedida` int(11) NOT NULL,
  `categoria_idcategoria` int(11) NOT NULL,
  PRIMARY KEY (`idMaterial`,`unidadeMedida_idunidadeMedida`,`categoria_idcategoria`),
  KEY `fk_material_unidadeMedida_idx` (`unidadeMedida_idunidadeMedida`),
  KEY `fk_material_categoria1_idx` (`categoria_idcategoria`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `saida`
--

CREATE TABLE IF NOT EXISTS `saida` (
  `idSaida` int(11) NOT NULL AUTO_INCREMENT,
  `quantidade` int(11) NOT NULL,
  `dataSaida` varchar(10) NOT NULL,
  `solicitante_id` int(11) NOT NULL,
  `material_idMaterial` varchar(45) NOT NULL,
  PRIMARY KEY (`idSaida`,`solicitante_id`,`material_idMaterial`),
  KEY `fk_saida_solicitante1_idx` (`solicitante_id`),
  KEY `fk_saida_material1_idx` (`material_idMaterial`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=53 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `solicitante`
--

CREATE TABLE IF NOT EXISTS `solicitante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `setor` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipoaquisicao`
--

CREATE TABLE IF NOT EXISTS `tipoaquisicao` (
  `idtipoAquisicao` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtipoAquisicao`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `tipoaquisicao`
--

INSERT INTO `tipoaquisicao` (`idtipoAquisicao`, `descricao`) VALUES
(1, 'DOAÇÃO'),
(2, 'COMPRA');

-- --------------------------------------------------------

--
-- Estrutura da tabela `unidadeMedida`
--

CREATE TABLE IF NOT EXISTS `unidadeMedida` (
  `idunidadeMedida` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(45) NOT NULL,
  PRIMARY KEY (`idunidadeMedida`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
